import 'package:lose_belly_fat_app/pages/detail/daily_exercise_add.dart';
import 'package:lose_belly_fat_app/pages/detail/daily_exercise_detail.dart';
import 'package:lose_belly_fat_app/pages/detail/daily_exercise_edit.dart';
import 'package:lose_belly_fat_app/pages/detail/exercise_add_new.dart';
import 'package:lose_belly_fat_app/pages/detail/exercise_item_detail.dart';
import 'package:lose_belly_fat_app/pages/detail/exercise_item_replace.dart';
import 'package:lose_belly_fat_app/pages/detail/exercise_practice_start.dart';
import 'package:lose_belly_fat_app/pages/discover_screen/dis_exercise_detail.dart';
import 'package:lose_belly_fat_app/pages/discover_screen/dis_none_exercise.dart';
import 'package:lose_belly_fat_app/pages/discover_screen/index.dart';
import 'package:lose_belly_fat_app/pages/home/index.dart';
import 'package:lose_belly_fat_app/pages/splash/Splash.dart';
import 'package:lose_belly_fat_app/pages/splash/choose_gender.dart';
import 'package:lose_belly_fat_app/pages/splash/reminder.dart';
import 'package:lose_belly_fat_app/pages/splash/selected.dart';
import 'package:lose_belly_fat_app/pages/traning_screen/components/select_training.dart';

class AppRouters {
  static const splash = '/splash';
  static const choosegender = '/choosegender';
  static const selected = '/selected';
  static const selectedtraining = '/selectedtraining';
  static const reminder = '/reminder';
  static const home = '/home';
  static const exercisedetail = '/daily-exercises';
  static const exsercisedetailedit = '/daily-exseercises-edit';
  static const exerciseitemreplace = '/exerciseitemreplace';
  static const exerciseitemdetail = '/exerciseitemdetail';
  static const exsercisedailyadd = '/exsercisedailyadd';
  static const exerciseaddnew = '/exerciseaddnew';
  static const exercisepracticestart = '/exercisepracticestart';
  static const dispage = '/dispage';
  static const disexercisedetai = '/disexercisedetail';
  static const disexercisedownload = '/disexerdownload';

  static final pages = {
    AppRouters.home: (context) => const HomePage(),
    AppRouters.splash: (context) => const Splash(),
    AppRouters.choosegender: (context) => const GenderChoose(),
    AppRouters.selected: (context) => const SelecredPlan(),
    AppRouters.selectedtraining: (context) => const SelectedTraining(),
    AppRouters.reminder: (context) => const SetReminder(),
    AppRouters.exercisedetail: (context) => const DailyExercisesDetail(),
    AppRouters.exsercisedetailedit: (context) =>
        const DailyExcercisesDetailEdit(),
    AppRouters.exerciseitemreplace: (context) => const ExserciseItemReplace(),
    AppRouters.exerciseitemdetail: (context) => const ExerciseItemDetail(),
    AppRouters.exsercisedailyadd: (context) => const ExerciseDailyAdd(),
    AppRouters.exerciseaddnew: (context) => const ExerciseAddNew(),
    AppRouters.exercisepracticestart: (context) =>
        const ExercisePracticeStart(),
    AppRouters.dispage: (context) => const DiscoverPage(),
    AppRouters.disexercisedetai: (context) => const DiscoverExerciseDetail(),
    AppRouters.disexercisedownload: (context) =>
        const DiscoverExerciseDownload(),
  };
}
