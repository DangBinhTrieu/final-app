import 'package:flutter/material.dart';

class NavigationPage extends StatelessWidget {
  const NavigationPage(
      {super.key, required this.currentIndex, this.onTapNavigation});
  final int currentIndex;
  final Function(int)? onTapNavigation;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.08,
      child: BottomNavigationBar(
        elevation: 0,
        currentIndex: currentIndex,
        selectedItemColor: Colors.black,
        selectedLabelStyle: const TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w600,
        ),
        unselectedLabelStyle: const TextStyle(
          color: Colors.black38,
          fontWeight: FontWeight.w600,
        ),
        unselectedItemColor: Colors.black38,
        showUnselectedLabels: true,
        selectedFontSize: 12,
        unselectedFontSize: 12,
        onTap: onTapNavigation,
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.bolt_rounded), label: "Traning"),
          BottomNavigationBarItem(
              icon: Icon(Icons.explore_rounded), label: "Discover"),
          BottomNavigationBarItem(
              icon: Icon(Icons.equalizer_rounded), label: "Reports"),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle), label: "Settings"),
        ],
      ),
    );
  }
}
