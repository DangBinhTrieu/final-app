class ExercisesItem {
  String? sId;
  String? name;
  List<String>? durian;
  String? description;
  String? animation;
  String? video;
  String? exerciseType;

  ExercisesItem(
      {this.sId,
      this.name,
      this.durian,
      this.description,
      this.animation,
      this.video,
      this.exerciseType});

  ExercisesItem.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    durian = json['durian'].cast<String>();
    description = json['description'];
    animation = json['animation'];
    video = json['video'];
    exerciseType = json['exerciseType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['name'] = name;
    data['durian'] = durian;
    data['description'] = description;
    data['animation'] = animation;
    data['video'] = video;
    data['exerciseType'] = exerciseType;
    return data;
  }
}
