import 'dart:convert';

import 'package:lose_belly_fat_app/api/model/exercise_item.dart';
import 'package:http/http.dart' as http;

class ExerciseItemController {
  static const url = 'http://localhost:8000';
  Future<List<ExercisesItem>> getExerciseItemList() async {
    var uri = Uri.parse('$url/v1/exercise');
    var res = await http.get(uri);
    if (res.statusCode == 200) {
      var data = jsonDecode(res.body);
      List<ExercisesItem> exerciseItemList = [];
      for (var element in data) {
        exerciseItemList.add(ExercisesItem.fromJson(element));
      }
      // print(exerciseItemList);
      return exerciseItemList;
    } else {
      throw Exception('Not found data');
    }
  }
}
