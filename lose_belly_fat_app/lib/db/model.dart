import 'package:flutter/material.dart';

class Exercise {
  int id;
  String name;
  String repeat;
  String animation;
  String video;
  String description;

  Exercise(this.id, this.name, this.repeat, this.animation, this.video,
      this.description);
}

class BeginnerItem {
  int id;
  String text;
  String images;
  List<Color> color;
  BeginnerItem(this.id, this.text, this.images, this.color);
}

//===========================================
class DisLessonExercise {
  int id;
  String name;
  int minus;
  String type;
  String des;
  Color? color;
  String images;
  DisLessonExercise(this.id, this.name, this.minus, this.type, this.des,
      this.color, this.images);
}
