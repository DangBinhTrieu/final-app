import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/db/model.dart';

import '../model/day_exercise.dart';
import '../model/fitness_goal.dart';

List<FitnessGoal> fitnessGoals = [
  FitnessGoal(
      title: 'Lose Belly Fat',
      level: 'Beginner',
      color: Color(0xFF1976D2),
      image: 'banner-1',
      dayExercises: [
        DayExercise(
          id: 2,
          exercises: [
            Exercise(1, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif",
                "video", "description"),
            Exercise(2, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif",
                "video", "description"),
          ],
          kcal: 107,
          netDuration: const Duration(seconds: 24),
        ),
        DayExercise(
          id: 3,
          exercises: [
            Exercise(1, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif",
                "video", "description"),
          ],
          kcal: 103,
          netDuration: const Duration(seconds: 12),
        ),
        DayExercise(
          id: 4,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 18),
        ),
        DayExercise(
          id: 5,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 12),
        ),
        DayExercise(
          id: 6,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 18),
        ),
        DayExercise(
          id: 7,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 20),
        ),
        DayExercise(
          id: 8,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 10),
        ),
      ]),
  FitnessGoal(
      title: 'Keep Fit',
      level: 'Intermediate',
      color: Color(0xFF1B06D2),
      image: 'banner-2',
      dayExercises: [
        DayExercise(
          id: 2,
          exercises: [
            Exercise(1, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif",
                "video", "description"),
          ],
          kcal: 107,
          netDuration: const Duration(seconds: 24),
        ),
        DayExercise(
          id: 3,
          exercises: [
            Exercise(1, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif",
                "video", "description"),
          ],
          kcal: 103,
          netDuration: const Duration(seconds: 12),
        ),
        DayExercise(
          id: 4,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 18),
        ),
        DayExercise(
          id: 5,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 12),
        ),
        DayExercise(
          id: 6,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 18),
        ),
        DayExercise(
          id: 7,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 20),
        ),
        DayExercise(
          id: 8,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 10),
        ),
      ]),
  FitnessGoal(
      title: 'Six Pack Abs',
      level: 'Advanced',
      color: Color(0xFF660285),
      image: 'banner-3',
      dayExercises: [
        DayExercise(
          id: 2,
          exercises: [
            Exercise(1, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif",
                "video", "description"),
          ],
          kcal: 107,
          netDuration: const Duration(seconds: 24),
        ),
        DayExercise(
          id: 3,
          exercises: [
            Exercise(1, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif",
                "video", "description"),
          ],
          kcal: 103,
          netDuration: const Duration(seconds: 12),
        ),
        DayExercise(
          id: 4,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 18),
        ),
        DayExercise(
          id: 5,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 12),
        ),
        DayExercise(
          id: 6,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 18),
        ),
        DayExercise(
          id: 7,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 20),
        ),
        DayExercise(
          id: 8,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 10),
        ),
      ]),
  FitnessGoal(
      title: 'Rock Hard Abs',
      level: 'Advanced',
      color: Color(0xFFC408CE),
      image: 'banner-3',
      dayExercises: [
        DayExercise(
          id: 2,
          exercises: [
            Exercise(1, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif",
                "video", "description"),
          ],
          kcal: 107,
          netDuration: const Duration(seconds: 24),
        ),
        DayExercise(
          id: 3,
          exercises: [
            Exercise(1, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif",
                "video", "description"),
          ],
          kcal: 103,
          netDuration: const Duration(seconds: 12),
        ),
        DayExercise(
          id: 4,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 18),
        ),
        DayExercise(
          id: 5,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 12),
        ),
        DayExercise(
          id: 6,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 18),
        ),
        DayExercise(
          id: 7,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 20),
        ),
        DayExercise(
          id: 8,
          exercises: [],
          kcal: 103,
          netDuration: const Duration(seconds: 10),
        ),
      ]),
];
