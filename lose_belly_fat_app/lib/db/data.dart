import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/db/model.dart';

// ignore: non_constant_identifier_names
List<Exercise> exercise_day_one = [
  Exercise(1, "JUMPING JACKS", "40", "ABDOMINAL_CRUNCHES.gif", "video",
      "description"),
  Exercise(
      2, "HIGH STEPPING", "30", "BICYCLE_CRUNCHES.gif", "video", "description"),
  Exercise(3, "STANDING BICYCLE CRUNCHES", "36", "BRIDGE.gif", "video",
      "description"),
  Exercise(
      4, "CROSS ARM CRUNCHES", "36", "BUTT_BRIDGE.gif", "video", "description"),
  Exercise(5, "SUPERMAN", "36", "clapping_crunch.gif", "video", "description"),
  Exercise(6, "REVERSE CRUNCHES", "36", "COBRAS.gif", "video", "description"),
  Exercise(
      7, "LEG RAISES", "36", "CROSS_ARM_CRUNCHES.gif", "video", "description"),
  Exercise(8, "SKIPPING WITHOUT ROPE", "40", "CRUNCH_KICKS.gif", "video",
      "description"),
  Exercise(9, "STANDING BICYCLE CRUCHES", "30", "DEAD_BUG.gif", "video",
      "description"),
  Exercise(10, "CROSS ARM CRUNCHES", "36", "DONKEY_KICKS_LEFT.gif", "video",
      "description"),
  Exercise(
      11, "SUPERMAN", "36", "DONKEY_KICKS_RIGHT.gif", "video", "description"),
  Exercise(12, "REVERSE CRUNCHES", "36", "FIRE_HYDRANT_LEFT.gif", "video",
      "description"),
  Exercise(
      13, "LEG RAISE", "36", "FIRE_HYDRANT_RIGHT.gif", "video", "description"),
  Exercise(14, "PLANK", "36", "FLUTTER_KICKS.gif", "video", "description"),
  Exercise(15, "SEATED SIDE BEND LEFT", "36", "HEEL_TOUCH.gif", "video",
      "description"),
  Exercise(16, "SEATED SIDE BEND RIGHT", "36", "INCHWORM.gif", "video",
      "description"),
  Exercise(17, "COBRA STRETCH", "36", "LEG_RAISEs.gif", "video", "description"),
  Exercise(18, "CHILD'S POSE", "36", "LUNGES.gif", "video", "description"),
];

// ignore: non_constant_identifier_names
List<Exercise> exercise_list = [
  Exercise(1, "ABDOMINAL CRUNCHES", "10", "ABDOMINAL_CRUNCHES.gif", "video",
      "description"),
  Exercise(2, "LEG RAISES", "10", "LEG_RAISES.gif", "video", "description"),
  Exercise(3, "PLANK", "20", "PLANK.gif", "video", "description"),
  Exercise(4, "REVERSE CRUNCHES", "10", "REVERSE_CRUNCHES.gif", "video",
      "description"),
  Exercise(5, "LONG ARM CRUNCHES", "10", "LONG_ARM_CRUNCHES.gif", "video",
      "description"),
  Exercise(
      6, "RUSSIAN TWIST", "10", "RUSSIAN_TWIST.gif", "video", "description"),
  Exercise(7, "BICYCLE CRUNCHES", "10", "BICYCLE_CRUNCHES.gif", "video",
      "description"),
  Exercise(8, "COBRAS", "10", "COBRAS.gif", "video", "description"),
  Exercise(9, "BRIDGE", "20", "BRIDGE.gif", "video", "description"),
  Exercise(10, "CROSS ARM CRUNCHES", "10", "CROSS_ARM_CRUNCHES.gif", "video",
      "description"),
  Exercise(11, "STRAIGHT-ARM PLANK", "20", "STRAIGHT_ARM_PLANK.gif", "video",
      "description"),
  Exercise(
      12, "WALL PUSH-UPS", "10", "WALL_PUSH_UPS.gif", "video", "description"),
  Exercise(13, "SQUATS", "10", "SQUATS.gif", "video", "description"),
  Exercise(14, "SIDE LUNGES", "10", "SIDE_LUNGES.webp", "video", "description"),
  Exercise(15, "LUNGES", "10", "LUNGES.gif", "video", "description"),
  Exercise(16, "SIDE PLANK RIGHT", "20", "SIDE_PLANK_RIGHT.gif", "video",
      "description"),
  Exercise(17, "SIDE PLANK LEFT", "10", "SIDE_PLANK_LEFT.gif", "video",
      "description"),
  Exercise(18, "BUTT BRIDGE", "10", "BUTT_BRIDGE.gif", "video", "description"),
  Exercise(19, "DONKEY KICKS LEFT", "10", "DONKEY_KICKS_LEFT.gif", "video",
      "description"),
  Exercise(20, "DONKEY KICKS RIGHT", "10", "DONKEY_KICKS_RIGHT.gif", "video",
      "description"),
];

// ignore: non_constant_identifier_names
List<Exercise> warm_up_list = [
  Exercise(1, "MOUNTAIN CLIMBER", "10", "Mountaion_Clumber.gif", "video",
      "description"),
  Exercise(
      2, "JUMPING JACKS", "20", "JUMPING_JACK.gif", "video", "description"),
  Exercise(3, "BURPEES", "10", "BURPEES.gif", "video", "description"),
  Exercise(
      4, "HIGH STEEPING", "20", "HIGH_STEPPING.gif", "video", "description"),
  Exercise(
      5, "JUMPING SQUATS", "10", "JUMPING_SQUATS.gif", "video", "description"),
  Exercise(6, "BUTT KICKS", "20", "BUTT_KICKS.gif", "video", "description"),
  Exercise(7, "CLAPS OVER HEAD", "10", "CLAPS_OVER_HEAD.gif", "video",
      "description"),
  Exercise(8, "SKIPPING WITHOUT ROPE", "20", "SKIPPING_WITHOUT_ROPE.gif",
      "video", "description"),
  Exercise(9, "CROSSBODY MOUNTAIN CLIMBER", "10",
      "CROSSBODY_MOUNTAIN_CLIMBER.gif", "video", "description"),
  Exercise(10, "CROSSBODY MOUNTAIN CLIMBER", "10",
      "CROSSBODY_MOUNTAIN_CLIMBER.gif", "video", "description"),
];

/// ===========================================================

class DayExercise {
  late int? id;
  late Duration? netDuration;
  DayExercise({required this.id, required this.netDuration});
}

/// ==================================================

List<BeginnerItem> beginnerList = [
  BeginnerItem(
    1,
    'No crunches',
    'person.svg',
    [
      const Color(0xFF3EA1EC),
      const Color(0xFF072EF4),
      const Color(0xFF3EA1EC),
    ],
  ),
  BeginnerItem(
    2,
    'Belly fat burner',
    'person.svg',
    [
      const Color(0xFFED6227),
      const Color(0xFFEA9413),
      const Color(0xFFF05E20),
    ],
  ),
  BeginnerItem(
    3,
    '3 min butt workout',
    'person.svg',
    [
      const Color(0xFF3EA1EC),
      const Color(0xFF072EF4),
      const Color(0xFF3EA1EC),
    ],
  ),
  BeginnerItem(
    4,
    'No push-up',
    'person.svg',
    [
      const Color(0xFFED6227),
      const Color(0xFFEA9413),
      const Color(0xFFF05E20),
    ],
  ),
  BeginnerItem(
    5,
    'No jumping',
    'person.svg',
    [
      const Color(0xFF3EA1EC),
      const Color(0xFF072EF4),
      const Color(0xFF3EA1EC),
    ],
  ),
];

/// =======================================
List<BeginnerItem> flexrelaxList = [
  BeginnerItem(
    1,
    'Sleepy time stretching',
    'sun.svg',
    [
      const Color(0xFF9318EB),
      const Color(0xFF300678),
      const Color(0xFFE13AF7),
    ],
  ),
  BeginnerItem(
    2,
    'Morning warm up',
    'sun.svg',
    [
      const Color(0xFFF4E926),
      const Color(0xFFF1AA24),
      const Color(0xFFDFF442),
    ],
  ),
  BeginnerItem(
    3,
    'Full body',
    'sun.svg',
    [
      const Color(0xFF26E3F4),
      const Color(0xFF0A6CCD),
      const Color(0xFF109BCA),
    ],
  ),
  BeginnerItem(
    4,
    'Neck & shou',
    'sun.svg',
    [
      const Color(0xFFF126F4),
      const Color(0xFFC0139D),
      const Color(0xFFF442EE),
    ],
  ),
];
//=======================================

List<DisLessonExercise> disLessonExer = [
  DisLessonExercise(
    1,
    'Belly fat buner HIIT beginner',
    14,
    'Beginner',
    'High-intensity belly fat burning wokouts with after effect. Keep burning calories after training!',
    Colors.blue,
    'banner1.png',
  ),
  DisLessonExercise(
    2,
    'Belly fat buner HIIT intermediate',
    18,
    'Intermediate',
    'High-intensity belly fat burning wokouts with after effect. Keep burning calories after training!',
    Color(0xFFEA6B04),
    'banner2.png',
  ),
  DisLessonExercise(
    3,
    'Belly fat buner HIIT advanced',
    20,
    'Advanced',
    'High-intensity belly fat burning wokouts with after effect. Keep burning calories after training!',
    Color(0xFFDB0C9D),
    'banner3.png',
  ),
  DisLessonExercise(
    4,
    'Bikini workout',
    7,
    'Beginner',
    'Most effective full body workouts! Take just 7 min a day to gain a more attraactive body with a flat belly, a round booty, and toned legs',
    Color(0xFFCB880B),
    'banner4.png',
  ),
  DisLessonExercise(
    5,
    '7 min classic',
    7,
    'Beginner',
    'Most effective full body workouts! Take just 7 min a day to gain a more attraactive body with a flat belly, a round booty, and toned legs',
    Color(0xFFCB880B),
    'banner4.png',
  ),
  DisLessonExercise(
    6,
    'Brutal ladder HIIT',
    20,
    'Advanced',
    'High-intensity belly fat burning wokouts with after effect. Keep burning calories after training!',
    Color(0xFFE11075),
    'banner3.png',
  ),
  DisLessonExercise(
    7,
    'HIIT intermeduate',
    13,
    'Intermediate',
    'High-intensity belly fat burning wokouts with after effect. Keep burning calories after training!',
    Color(0xFFE110A6),
    'banner2.png',
  ),
  DisLessonExercise(
    8,
    'Sleeping time stretching',
    8,
    '',
    'Relax yourself and get a high-quality sleep',
    Color(0xFF320576),
    'banner1.png',
  ),
  DisLessonExercise(
    9,
    'Morning warm up',
    6,
    '',
    'Wake up with energy, make your body primed for the day',
    Color(0xFFEF720C),
    'banner2.png',
  ),
  DisLessonExercise(
    10,
    'Full body stretching',
    7,
    '',
    'Relax your body and improve range of motion It\'s also a good stretching rountine for running and exercise.',
    Color(0xFFE8800A),
    'banner1.png',
  ),
];
