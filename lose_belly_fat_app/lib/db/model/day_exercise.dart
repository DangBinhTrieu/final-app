import 'package:lose_belly_fat_app/db/model.dart';

class DayExercise {
  late int id;
  late int kcal;
  late int progress;
  late Duration netDuration;
  late List<Exercise> exercises;
  DayExercise(
      {required this.id,
      required this.exercises,
      required this.kcal,
      required this.netDuration,
      this.progress = 0});
}
