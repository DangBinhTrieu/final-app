import 'package:flutter/material.dart';

import 'day_exercise.dart';

class FitnessGoal {
  String title;
  String level;
  Color color;
  int progress;
  String image;
  List<DayExercise> dayExercises;

  FitnessGoal({
    required this.title,
    required this.level,
    required this.color,
    required this.image,
    this.progress = 30,
    required this.dayExercises,
  });
}
