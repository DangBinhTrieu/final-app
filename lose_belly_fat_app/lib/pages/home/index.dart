import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/navigate/app_navigation.dart';
import 'package:lose_belly_fat_app/pages/discover_screen/index.dart';
import 'package:lose_belly_fat_app/pages/reports_screen/index.dart';
import 'package:lose_belly_fat_app/pages/settings_screen/index.dart';
import 'package:lose_belly_fat_app/pages/traning_screen/index.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Widget?> screens = [
    const TraningPage(),
    const DiscoverPage(),
    const ReportsPage(),
    const SettingsPage(),
  ];
  late int currentIndex;

  @override
  void initState() {
    super.initState();
    currentIndex = 0;
  }

  void onTapNavigation(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar: NavigationPage(
        currentIndex: currentIndex,
        onTapNavigation: onTapNavigation,
      ),
    );
  }
}
