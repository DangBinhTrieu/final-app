import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class BoxCalory extends StatefulWidget {
  const BoxCalory({
    super.key,
    required this.mediaQuery,
  });

  final Size mediaQuery;

  @override
  State<BoxCalory> createState() => _BoxCaloryState();
}

class _BoxCaloryState extends State<BoxCalory> {
  bool showAvg = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.mediaQuery.width,
      height: widget.mediaQuery.height * 0.4,
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.black38),
          borderRadius: const BorderRadius.all(Radius.circular(10))),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          const Positioned(
            top: 0,
            left: 10,
            child: Row(
              children: [
                Text(
                  "Calories burned, estimated ",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "(Kcal)",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                    color: Colors.black26,
                  ),
                )
              ],
            ),
          ),
          AspectRatio(
            aspectRatio: 1.70,
            child: LineChart(
              showAvg ? avgData() : mainData(),
            ),
          ),
          SizedBox(
            width: 60,
            height: 34,
            child: TextButton(
              onPressed: () {
                setState(() {
                  showAvg = !showAvg;
                });
              },
              child: Text(
                'avg',
                style: TextStyle(
                  fontSize: 12,
                  color: showAvg ? Colors.white.withOpacity(0.5) : Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget bottomTitleWidgets(double value, TitleMeta meta) {
  const style = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 16,
  );
  Widget text;
  switch (value.toInt()) {
    case 1:
      text = const Text(
        '15',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 3:
      text = const Text(
        '17',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 4:
      text = const Text(
        '19',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 5:
      text = const Text(
        '20',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 6:
      text = const Text(
        '21',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 7:
      text = const Text(
        '23',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 8:
      text = const Text(
        '25',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 10:
      text = const Text(
        '27',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    default:
      text = const Text(
        '',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
  }

  return SideTitleWidget(
    axisSide: meta.axisSide,
    child: text,
  );
}

Widget leftTitleWidgets(double value, TitleMeta meta) {
  const style = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 15,
  );
  String text;
  switch (value.toInt()) {
    case 1:
      text = '0';
      break;
    case 2:
      text = '40';
      break;
    case 3:
      text = '80';
      break;
    case 4:
      text = '120';
      break;
    case 5:
      text = '160';
      break;
    case 6:
      text = '200';
      break;
    default:
      return Container();
  }

  return Text(
    text,
    style: const TextStyle(
      fontSize: 12,
      color: Colors.black54,
    ),
    textAlign: TextAlign.left,
  );
}

LineChartData mainData() {
  return LineChartData(
    gridData: FlGridData(
      show: true,
      drawVerticalLine: true,
      horizontalInterval: 1,
      getDrawingHorizontalLine: (value) {
        return const FlLine(
          color: Colors.black38,
          strokeWidth: 1,
        );
      },
    ),
    titlesData: const FlTitlesData(
      show: true,
      rightTitles: AxisTitles(
        sideTitles: SideTitles(showTitles: false),
      ),
      topTitles: AxisTitles(
        sideTitles: SideTitles(showTitles: false),
      ),
      bottomTitles: AxisTitles(
        sideTitles: SideTitles(
          showTitles: true,
          reservedSize: 30,
          interval: 1,
          getTitlesWidget: bottomTitleWidgets,
        ),
      ),
      leftTitles: AxisTitles(
        sideTitles: SideTitles(
          showTitles: true,
          interval: 1,
          getTitlesWidget: leftTitleWidgets,
          reservedSize: 42,
        ),
      ),
    ),
    borderData: FlBorderData(
      show: false,
      border: Border.all(color: Color(0xFFE4EAEF)),
    ),
    minX: 0,
    maxX: 11,
    minY: 0,
    maxY: 6,
    lineBarsData: [
      LineChartBarData(
        spots: const [
          // FlSpot(0, 3),
          // FlSpot(2.6, 2),
          // FlSpot(4.9, 5),
          // FlSpot(6.8, 3.1),
          // FlSpot(8, 4),
          // FlSpot(9.5, 3),
          // FlSpot(11, 4),
        ],
        isCurved: true,
        // color: Color(0xFFDC0A3B),
        barWidth: 10,
        isStrokeCapRound: true,
        dotData: const FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: true, color: Color(0x4CF378B9),
          // gradient: LinearGradient(
          //   begin: Color(0xFF90CAF9),
          //   end: Col
          // ),
        ),
      ),
    ],
  );
}

LineChartData avgData() {
  return LineChartData(
    lineTouchData: const LineTouchData(enabled: false),
    gridData: FlGridData(
      show: true,
      drawHorizontalLine: true,
      verticalInterval: 2,
      horizontalInterval: 1,
      getDrawingVerticalLine: (value) {
        return const FlLine(
          color: Color(0xAAE75CBD),
          strokeWidth: 1,
        );
      },
      getDrawingHorizontalLine: (value) {
        return const FlLine(
          color: Color(0xAAE75CBD),
          strokeWidth: 1,
        );
      },
    ),
    titlesData: const FlTitlesData(
      show: true,
      bottomTitles: AxisTitles(
        sideTitles: SideTitles(
          showTitles: true,
          reservedSize: 30,
          getTitlesWidget: bottomTitleWidgets,
          interval: 1,
        ),
      ),
      leftTitles: AxisTitles(
        sideTitles: SideTitles(
          showTitles: true,
          getTitlesWidget: leftTitleWidgets,
          reservedSize: 42,
          interval: 1,
        ),
      ),
      topTitles: const AxisTitles(
        sideTitles: SideTitles(showTitles: false),
      ),
      rightTitles: const AxisTitles(
        sideTitles: SideTitles(showTitles: false),
      ),
    ),
    borderData: FlBorderData(
      show: true,
      border: Border.all(color: const Color(0xff37434d)),
    ),
    minX: 0,
    maxX: 11,
    minY: 0,
    maxY: 6,
    lineBarsData: [
      LineChartBarData(
        spots: const [
          FlSpot(0, 3.44),
          FlSpot(2.6, 3.44),
          FlSpot(4.9, 3.44),
          FlSpot(6.8, 3.44),
          FlSpot(8, 3.44),
          FlSpot(9.5, 3.44),
          FlSpot(11, 3.44),
        ],
        isCurved: true,
        gradient: LinearGradient(
          colors: [
            ColorTween(begin: Color(0xFF2196F3), end: Color(0xFF2C6ADC))
                .lerp(0.2)!,
            ColorTween(begin: Color(0xFF2196F3), end: Color(0xFF2C6ADC))
                .lerp(0.2)!,
          ],
        ),
        barWidth: 5,
        isStrokeCapRound: true,
        dotData: const FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: true,
          gradient: LinearGradient(
            colors: [
              ColorTween(begin: Color(0xFF2196F3), end: Color(0xFF2C6ADC))
                  .lerp(0.2)!
                  .withOpacity(0.1),
              ColorTween(begin: Color(0xFF2196F3), end: Color(0xFF2C6ADC))
                  .lerp(0.2)!
                  .withOpacity(0.1),
            ],
          ),
        ),
      ),
    ],
  );
}
