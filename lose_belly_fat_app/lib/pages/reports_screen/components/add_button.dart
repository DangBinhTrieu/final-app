import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class AddButton extends StatefulWidget {
  const AddButton({
    super.key,
    required this.mediaQuery,
    required this.calBMI,
    required this.icon,
  });

  final Size mediaQuery;
  final bool calBMI;
  final Widget icon;

  @override
  State<AddButton> createState() => _AddButtonState();
}

class _AddButtonState extends State<AddButton> {
  CalendarFormat format = CalendarFormat.week;
  DateTime selectedDay = DateTime.now();
  DateTime focusedDay = DateTime.now();
  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        showDialog(
          context: context,
          barrierColor: Colors.black.withOpacity(0.6),
          builder: (context) => StatefulBuilder(
            builder: (context, setState) => LayoutBuilder(
              builder: (context, constraints) {
                return Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Dialog(
                    insetPadding: const EdgeInsets.all(8.0),
                    shape: const RoundedRectangleBorder(
                        side: BorderSide.none, borderRadius: BorderRadius.zero),
                    child: Container(
                      width: widget.mediaQuery.width,
                      height: widget.mediaQuery.height * 0.35,
                      padding: const EdgeInsets.all(8.0),
                      decoration: const BoxDecoration(color: Colors.white),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Positioned(
                            bottom: 0,
                            right: 5,
                            child: Row(
                              children: [
                                TextButton(
                                  onPressed: () {},
                                  child: const Text(
                                    "CANCEL",
                                    style: TextStyle(
                                      color: Colors.pink,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {},
                                  child: const Text(
                                    "SAVE",
                                    style: TextStyle(
                                      color: Colors.pink,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          widget.calBMI
                              ? Column(
                                  children: [
                                    TableCalendar(
                                      rowHeight: 30,
                                      daysOfWeekHeight: 18,
                                      focusedDay: focusedDay,
                                      firstDay: DateTime(2000),
                                      lastDay: DateTime(2030),
                                      calendarFormat: format,
                                      startingDayOfWeek:
                                          StartingDayOfWeek.sunday,
                                      onFormatChanged: (format) {
                                        setState(() {
                                          format = format;
                                        });
                                      },
                                      selectedDayPredicate: (day) {
                                        return isSameDay(selectedDay, day);
                                      },
                                      calendarStyle: const CalendarStyle(
                                          selectedTextStyle: TextStyle(
                                              color: Colors.pink,
                                              fontWeight: FontWeight.w600),
                                          selectedDecoration: BoxDecoration(
                                            color: Colors.white,
                                          ),
                                          markerDecoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.black26)),
                                      headerStyle: const HeaderStyle(
                                        formatButtonVisible: false,
                                        formatButtonShowsNext: false,
                                        titleCentered: true,
                                        leftChevronVisible: true,
                                        rightChevronVisible: true,
                                        titleTextStyle: TextStyle(
                                            decoration: TextDecoration.none,
                                            color: Colors.black54),
                                      ),
                                    ),
                                    Positioned(
                                      child: Container(
                                        width: widget.mediaQuery.width,
                                        height: widget.mediaQuery.height * 0.02,
                                        decoration: const BoxDecoration(
                                          border: Border(
                                            top: BorderSide(
                                              width: 1,
                                              color: Colors.black38,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 20,
                                      child: SizedBox(
                                        width: widget.mediaQuery.width * 0.9,
                                        height: widget.mediaQuery.height * 0.1,
                                        child: Row(
                                          children: [
                                            Expanded(
                                              flex: 4,
                                              child: Container(
                                                width: widget.mediaQuery.width *
                                                    0.6,
                                                height:
                                                    widget.mediaQuery.height *
                                                        0.04,
                                                decoration: const BoxDecoration(
                                                    border: Border(
                                                        bottom: BorderSide(
                                                  width: 1,
                                                  color: Colors.pink,
                                                ))),
                                                child: const TextField(
                                                  style: TextStyle(
                                                      fontSize: 30,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Colors.black),
                                                  decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                            bottom: 120,
                                                            left: 20),
                                                    border: OutlineInputBorder(
                                                      borderSide:
                                                          BorderSide.none,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 1,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: Container(
                                                  width: 20,
                                                  height: 30,
                                                  decoration:
                                                      const BoxDecoration(
                                                          color: Colors.pink,
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          6))),
                                                  child: const Center(
                                                    child: Text(
                                                      "CM",
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            const Expanded(
                                              flex: 1,
                                              child: Text("IN"),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Text(
                                        'Weight',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 26,
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 20,
                                        child: SizedBox(
                                          width: widget.mediaQuery.width * 0.9,
                                          height:
                                              widget.mediaQuery.height * 0.08,
                                          child: Row(
                                            children: [
                                              Expanded(
                                                flex: 4,
                                                child: Container(
                                                  width:
                                                      widget.mediaQuery.width *
                                                          0.6,
                                                  height:
                                                      widget.mediaQuery.height *
                                                          0.04,
                                                  decoration:
                                                      const BoxDecoration(
                                                          border: Border(
                                                              bottom:
                                                                  BorderSide(
                                                    width: 1,
                                                    color: Colors.pink,
                                                  ))),
                                                  child: const TextField(
                                                    style: TextStyle(
                                                        fontSize: 30,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colors.black),
                                                    decoration: InputDecoration(
                                                      contentPadding:
                                                          EdgeInsets.only(
                                                              bottom: 120,
                                                              left: 20),
                                                      border:
                                                          OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Padding(
                                                  padding: const EdgeInsets.all(
                                                      10.0),
                                                  child: Container(
                                                    width: 20,
                                                    height: 30,
                                                    decoration:
                                                        const BoxDecoration(
                                                            color: Colors.pink,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            6))),
                                                    child: const Center(
                                                      child: Text(
                                                        "KG",
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              const Expanded(
                                                flex: 1,
                                                child: Text("LB"),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const Text(
                                        'Heigh',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 26,
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 20,
                                        child: SizedBox(
                                          width: widget.mediaQuery.width * 0.9,
                                          height:
                                              widget.mediaQuery.height * 0.08,
                                          child: Row(
                                            children: [
                                              Expanded(
                                                flex: 4,
                                                child: Container(
                                                  width:
                                                      widget.mediaQuery.width *
                                                          0.6,
                                                  height:
                                                      widget.mediaQuery.height *
                                                          0.04,
                                                  decoration:
                                                      const BoxDecoration(
                                                          border: Border(
                                                              bottom:
                                                                  BorderSide(
                                                    width: 1,
                                                    color: Colors.pink,
                                                  ))),
                                                  child: const TextField(
                                                    style: TextStyle(
                                                        fontSize: 30,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colors.black),
                                                    decoration: InputDecoration(
                                                      contentPadding:
                                                          EdgeInsets.only(
                                                              bottom: 120,
                                                              left: 20),
                                                      border:
                                                          OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Padding(
                                                  padding: const EdgeInsets.all(
                                                      10.0),
                                                  child: Container(
                                                    width: 20,
                                                    height: 30,
                                                    decoration:
                                                        const BoxDecoration(
                                                            color: Colors.pink,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            6))),
                                                    child: const Center(
                                                      child: Text(
                                                        "CM",
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              const Expanded(
                                                flex: 1,
                                                child: Text("IN"),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
      icon: widget.icon,
    );
  }
}
