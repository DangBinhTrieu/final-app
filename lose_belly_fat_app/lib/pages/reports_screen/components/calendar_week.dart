import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class BoxCalendarWeek extends StatefulWidget {
  const BoxCalendarWeek({
    super.key,
    required this.mediaQuery,
  });

  final Size mediaQuery;

  @override
  State<BoxCalendarWeek> createState() => _BoxCalendarWeekState();
}

class _BoxCalendarWeekState extends State<BoxCalendarWeek> {
  CalendarFormat format = CalendarFormat.week;
  DateTime selectedDay = DateTime.now();
  DateTime focusedDay = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.mediaQuery.width,
      height: widget.mediaQuery.height * 0.2,
      padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.black38),
          borderRadius: const BorderRadius.all(Radius.circular(10))),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          const Positioned(
            top: 0,
            left: 10,
            child: Text(
              "This week",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18,
                color: Colors.black,
              ),
            ),
          ),
          const Positioned(
            top: 0,
            right: 10,
            child: Row(
              children: [
                Text(
                  "HISTORY",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                    color: Colors.pink,
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 12,
                  color: Colors.black26,
                )
              ],
            ),
          ),
          const Positioned(
            bottom: 0,
            child: Row(
              children: [
                Text(
                  "0",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                    color: Colors.pink,
                  ),
                ),
                Text(
                  " DAY IN A ROW",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          ),
          TableCalendar(
            focusedDay: focusedDay,
            firstDay: DateTime(2000),
            lastDay: DateTime(2030),
            calendarFormat: format,
            startingDayOfWeek: StartingDayOfWeek.sunday,
            onFormatChanged: (format) {
              setState(() {
                format = format;
              });
            },
            selectedDayPredicate: (day) {
              return isSameDay(selectedDay, day);
            },
            calendarStyle: const CalendarStyle(
                selectedTextStyle:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                selectedDecoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.all(Radius.circular(100))),
                markerDecoration: BoxDecoration(
                    shape: BoxShape.circle, color: Colors.black26)),
            headerStyle: const HeaderStyle(
              formatButtonVisible: false,
              formatButtonShowsNext: false,
              titleCentered: true,
              leftChevronVisible: false,
              rightChevronVisible: false,
              titleTextStyle: TextStyle(
                  decoration: TextDecoration.none, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
