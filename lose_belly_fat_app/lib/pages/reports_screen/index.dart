import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lose_belly_fat_app/pages/reports_screen/components/add_button.dart';
import 'package:lose_belly_fat_app/pages/reports_screen/components/box_calory.dart';
import 'package:lose_belly_fat_app/pages/reports_screen/components/box_header.dart';
import 'package:lose_belly_fat_app/pages/reports_screen/components/box_waistline.dart';
import 'package:lose_belly_fat_app/pages/reports_screen/components/calendar_week.dart';

class ReportsPage extends StatefulWidget {
  const ReportsPage({super.key});

  @override
  State<ReportsPage> createState() => _ReportsPageState();
}

class _ReportsPageState extends State<ReportsPage> {
  bool showAvg = false;
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Container(
      width: mediaQuery.width,
      height: mediaQuery.height,
      padding: const EdgeInsets.all(10.0),
      decoration: const BoxDecoration(
        color: Color(0xFFF6F6F6),
      ),
      child: Stack(
        children: [
          Container(
            width: mediaQuery.width,
            height: mediaQuery.height * 0.5,
            decoration: const BoxDecoration(
              color: Colors.black,
            ),
          ),
          Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.black,
              toolbarHeight: mediaQuery.height * 0.2,
              flexibleSpace: const Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Nice!",
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            body: ListView(
              children: [
                BoxHeader(mediaQuery: mediaQuery),
                BoxCalendarWeek(mediaQuery: mediaQuery),
                BoxCalory(mediaQuery: mediaQuery),
                BoxWaistline(
                  mediaQuery: mediaQuery,
                  title: 'Waistline',
                  subT: 'cm',
                  num1: '170.0',
                  num2: '0.0',
                  num3: '170.0',
                ),
                BoxWaistline(
                  mediaQuery: mediaQuery,
                  title: 'Weight',
                  subT: 'kg',
                  num1: '59.0',
                  num2: '1.0',
                  num3: '58.5.0',
                ),
                // BoxWaistline(mediaQuery: mediaQuery),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Container(
                    width: mediaQuery.width,
                    height: mediaQuery.height * 0.22,
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Colors.black38,
                      ),
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        const Positioned(
                          top: 0,
                          left: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "BMI(kg/m2) ",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    "BMI range and categories come from",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 12,
                                      color: Colors.black54,
                                    ),
                                  ),
                                  Text(
                                    " Wiki",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 12,
                                        color: Colors.blue,
                                        decorationColor: Colors.blue,
                                        decorationStyle:
                                            TextDecorationStyle.solid),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 10,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text(
                                "Edit",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,
                                  color: Colors.black38,
                                ),
                              ),
                              AddButton(
                                mediaQuery: mediaQuery,
                                calBMI: false,
                                icon: const Icon(
                                  Icons.edit,
                                  size: 18,
                                  color: Colors.black54,
                                ),
                              ),
                              // IconButton(
                              //   onPressed: () {},
                              //   icon: const Icon(
                              //     Icons.edit,
                              //     size: 20,
                              //     color: Colors.black38,
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                        Positioned(
                          bottom: 30,
                          child: Container(
                            width: mediaQuery.width * 0.8,
                            height: mediaQuery.height * 0.03,
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              gradient: LinearGradient(
                                colors: [
                                  Color(0xFF030374),
                                  Color(0xFF0A63E8),
                                  Color(0xFF0A63E8),
                                  Color(0xFF11D4BA),
                                  Color(0xFFBDDB13),
                                  Color(0xFFD6910F),
                                  Color(0xFFC32003),
                                  Color(0xFFC80707),
                                ],
                              ),
                            ),
                          ),
                        ),
                        AspectRatio(
                          aspectRatio: 1.70,
                          child: LineChart(
                            showAvg ? avgData() : mainData(),
                          ),
                        ),
                        SizedBox(
                          width: 60,
                          height: 34,
                          child: TextButton(
                            onPressed: () {
                              setState(() {
                                showAvg = !showAvg;
                              });
                            },
                            child: Text(
                              'avg',
                              style: TextStyle(
                                fontSize: 12,
                                color: showAvg
                                    ? Colors.white.withOpacity(0.5)
                                    : Colors.white,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const Positioned(
                          left: 0,
                          bottom: 70,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.circle,
                                size: 20,
                                color: Color(0xFF11D4BA),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Text(
                                'Healthy weight',
                                style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 14,
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          const Positioned(
            top: 10,
            right: 20,
            child: Icon(
              Icons.notifications,
              size: 20,
              color: Colors.amberAccent,
            ),
          ),
          Positioned(
            left: 20,
            top: mediaQuery.height * 0.15,
            child: const Row(
              children: [
                Text(
                  "Calendar",
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  "Data",
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Widget bottomTitleWidgets(double value, TitleMeta meta) {
  const style = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 13,
  );
  Widget text;
  switch (value.toInt()) {
    case 1:
      text = const Text(
        '15',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 2:
      text = const Text(
        '16',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 4:
      text = const Text(
        '18.5',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 7:
      text = const Text(
        '25',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 10:
      text = const Text(
        '30',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 13:
      text = const Text(
        '35',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    case 15:
      text = const Text(
        '40',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
    default:
      text = const Text(
        '',
        style: TextStyle(
          fontSize: 12,
          color: Colors.black54,
        ),
      );
      break;
  }

  return SideTitleWidget(
    axisSide: meta.axisSide,
    child: text,
  );
}

Widget leftTitleWidgets(double value, TitleMeta meta) {
  const style = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 15,
  );
  String text;
  switch (value.toInt()) {
    case 1:
      text = '';
      break;
    default:
      return Container();
  }

  return Text(
    text,
    style: const TextStyle(
      fontSize: 5,
      color: Colors.black54,
    ),
    textAlign: TextAlign.left,
  );
}

LineChartData mainData() {
  return LineChartData(
    gridData: FlGridData(
      show: false,
      drawVerticalLine: true,
      horizontalInterval: 1,
      // verticalInterval: 1,
      getDrawingHorizontalLine: (value) {
        return const FlLine(
          color: Colors.black38,
          strokeWidth: 1,
        );
      },
    ),
    titlesData: const FlTitlesData(
      show: true,
      rightTitles: AxisTitles(
        sideTitles: SideTitles(showTitles: false),
      ),
      topTitles: AxisTitles(
        sideTitles: SideTitles(showTitles: false),
      ),
      bottomTitles: AxisTitles(
        sideTitles: SideTitles(
          showTitles: true,
          reservedSize: 30,
          interval: 1,
          getTitlesWidget: bottomTitleWidgets,
        ),
      ),
      leftTitles: AxisTitles(
        sideTitles: SideTitles(
          showTitles: true,
          interval: 1,
          getTitlesWidget: leftTitleWidgets,
          reservedSize: 42,
        ),
      ),
    ),
    borderData: FlBorderData(
      show: false,
      border: Border.all(color: Color(0xFFE4EAEF)),
    ),
    minX: 0,
    maxX: 11,
    minY: 0,
    maxY: 6,
    lineBarsData: [
      LineChartBarData(
        spots: const [
          // FlSpot(0, 3),
          // FlSpot(2.6, 2),
          // FlSpot(4.9, 5),
          // FlSpot(6.8, 3.1),
          // FlSpot(8, 4),
          // FlSpot(9.5, 3),
          // FlSpot(11, 4),
        ],
        isCurved: true,
        color: Color(0xFFDC0A3B),
        barWidth: 5,
        isStrokeCapRound: true,
        dotData: const FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: true,
          color: Color(0x4CF378B9),
        ),
      ),
    ],
  );
}

LineChartData avgData() {
  return LineChartData(
    lineTouchData: const LineTouchData(enabled: false),
    gridData: FlGridData(
      show: true,
      drawHorizontalLine: true,
      verticalInterval: 1,
      horizontalInterval: 1,
      getDrawingVerticalLine: (value) {
        return const FlLine(
          color: Color(0xAAE75CBD),
          strokeWidth: 1,
        );
      },
      getDrawingHorizontalLine: (value) {
        return const FlLine(
          color: Color(0xAAE75CBD),
          strokeWidth: 1,
        );
      },
    ),
    titlesData: const FlTitlesData(
      show: true,
      bottomTitles: AxisTitles(
        sideTitles: SideTitles(
          showTitles: true,
          reservedSize: 30,
          getTitlesWidget: bottomTitleWidgets,
          interval: 1,
        ),
      ),
      leftTitles: AxisTitles(
        sideTitles: SideTitles(
          showTitles: true,
          getTitlesWidget: leftTitleWidgets,
          reservedSize: 42,
          interval: 1,
        ),
      ),
      topTitles: const AxisTitles(
        sideTitles: SideTitles(showTitles: false),
      ),
      rightTitles: const AxisTitles(
        sideTitles: SideTitles(showTitles: false),
      ),
    ),
    borderData: FlBorderData(
      show: true,
      border: Border.all(color: const Color(0xff37434d)),
    ),
    minX: 0,
    maxX: 11,
    minY: 0,
    maxY: 6,
    lineBarsData: [
      LineChartBarData(
        spots: const [
          FlSpot(0, 3.44),
          FlSpot(2.6, 3.44),
          FlSpot(4.9, 3.44),
          FlSpot(6.8, 3.44),
          FlSpot(8, 3.44),
          FlSpot(9.5, 3.44),
          FlSpot(11, 3.44),
        ],
        isCurved: true,
        gradient: LinearGradient(
          colors: [
            ColorTween(begin: Color(0xFF2196F3), end: Color(0xFF2C6ADC))
                .lerp(0.2)!,
            ColorTween(begin: Color(0xFF2196F3), end: Color(0xFF2C6ADC))
                .lerp(0.2)!,
          ],
        ),
        barWidth: 5,
        isStrokeCapRound: true,
        dotData: const FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: true,
          gradient: LinearGradient(
            colors: [
              ColorTween(begin: Color(0xFF2196F3), end: Color(0xFF2C6ADC))
                  .lerp(0.2)!
                  .withOpacity(0.1),
              ColorTween(begin: Color(0xFF2196F3), end: Color(0xFF2C6ADC))
                  .lerp(0.2)!
                  .withOpacity(0.1),
            ],
          ),
        ),
      ),
    ],
  );
}
