import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SelecredPlan extends StatelessWidget {
  const SelecredPlan({super.key});

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xFF030369),
      appBar: AppBar(
        backgroundColor: const Color(0xFF030369),
        title: IconButton(
          onPressed: () {},
          icon: const Icon(
            Icons.arrow_back,
            size: 20,
            color: Colors.white,
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        width: mediaQuery.width,
        height: mediaQuery.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: mediaQuery.width * 0.6,
              child: const Text(
                'Which plan do you want to start with?',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.16,
              decoration: BoxDecoration(
                color: Colors.blue,
                border: Border.all(width: 2, color: Colors.yellow),
                borderRadius: const BorderRadius.all(Radius.circular(10)),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 20,
                    left: 30,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        Text(
                          'Lose Belly Fat',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 5,
                    child: Image.asset(
                      './assets/images/banner2.png',
                      fit: BoxFit.cover,
                      repeat: ImageRepeat.noRepeat,
                      width: 130,
                      height: 130,
                    ),
                    // SvgPicture.asset(
                    //   'assets/svgs/boy.svg',
                    //   semanticsLabel: 'Image Splah SVG',
                    //   fit: BoxFit.cover,
                    //   width: 130,
                    //   height: 130,
                    // ),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: Container(
                      width: 100,
                      height: 30,
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      decoration: const BoxDecoration(
                        color: Colors.yellow,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.pushReplacementNamed(
                                  context, '/reminder');
                            },
                            child: const Icon(
                              Icons.check,
                              size: 18,
                              color: Colors.black,
                            ),
                          ),
                          const Text(
                            'Selected',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.16,
              decoration: const BoxDecoration(
                color: Color(0xFF1C29BB),
                // border: Border.all(width: 2, color: Colors.yellow),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 20,
                    left: 30,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        Text(
                          'Keep Fit',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 5,
                    child: Image.asset(
                      './assets/images/banner1.png',
                      fit: BoxFit.cover,
                      repeat: ImageRepeat.noRepeat,
                      width: 130,
                      height: 130,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.16,
              decoration: const BoxDecoration(
                color: Color(0xFF500363),
                // border: Border.all(width: 2, color: Colors.yellow),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 20,
                    left: 30,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        Text(
                          'Six Pack Abs',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 5,
                    child: Image.asset(
                      './assets/images/banner3.png',
                      fit: BoxFit.cover,
                      repeat: ImageRepeat.noRepeat,
                      width: 130,
                      height: 130,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.16,
              decoration: const BoxDecoration(
                color: Color(0xFF8E096B),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 20,
                    left: 30,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        Text(
                          'Rock Hard Abs',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 5,
                    child: Image.asset(
                      './assets/images/banner4.png',
                      fit: BoxFit.cover,
                      repeat: ImageRepeat.noRepeat,
                      width: 130,
                      height: 130,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
