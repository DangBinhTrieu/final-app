import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class GenderChoose extends StatelessWidget {
  const GenderChoose({super.key});

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xFF030369),
      body: SizedBox(
        width: mediaQuery.width,
        height: mediaQuery.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: mediaQuery.width * 0.9,
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'HI',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.w700,
                        fontFamily: AutofillHints.birthday),
                  ),
                  Text(
                    'Select Gender',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w600,
                        fontFamily: AutofillHints.birthday),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: mediaQuery.width * 0.8,
              height: mediaQuery.height * 0.3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        'assets/svgs/girl.svg',
                        semanticsLabel: 'Image Splah SVG',
                        fit: BoxFit.cover,
                        width: 100,
                        height: 100,
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Checkbox(
                            value: false,
                            onChanged: (value) {},
                            semanticLabel: 'Femalse',
                            checkColor: Colors.white,
                            focusColor: Colors.white,
                            shape: const CircleBorder(),
                            side: const BorderSide(
                              width: 2,
                              color: Color(0xFFFFFFFF),
                            ),
                          ),
                          const Text(
                            'Female',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                                fontFamily: AutofillHints.birthday),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        'assets/svgs/boy.svg',
                        semanticsLabel: 'Image Splah SVG',
                        fit: BoxFit.cover,
                        width: 100,
                        height: 100,
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Checkbox(
                            value: false,
                            onChanged: (value) {},
                            semanticLabel: 'Male',
                            checkColor: Colors.white,
                            focusColor: Colors.white,
                            shape: const CircleBorder(),
                            side: const BorderSide(
                              width: 2,
                              color: Color(0xFFFFFFFF),
                            ),
                          ),
                          const Text(
                            'Male',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                                fontFamily: AutofillHints.birthday),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              width: mediaQuery.width * 0.6,
              height: mediaQuery.height * 0.08,
              child: ElevatedButton(
                style: const ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(Color(0x0AEBEBEB)),
                  shape: MaterialStatePropertyAll(BeveledRectangleBorder()),
                ),
                onPressed: () {
                  Navigator.pushReplacementNamed(
                    context,
                    '/selected',
                  );
                },
                child: const Text(
                  'NEXT',
                  style: TextStyle(
                      color: Colors.white70,
                      fontSize: 24,
                      fontWeight: FontWeight.w600,
                      fontFamily: AutofillHints.birthday),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
