import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/components/appbar_default.dart';
import 'package:lose_belly_fat_app/components/appbar_default_dis.dart';
import 'package:lose_belly_fat_app/components/dot_border/dashed_rect.dart';

class DiscoverExerciseDownload extends StatefulWidget {
  const DiscoverExerciseDownload({super.key});

  @override
  State<DiscoverExerciseDownload> createState() =>
      _DiscoverExerciseDownloadState();
}

class _DiscoverExerciseDownloadState extends State<DiscoverExerciseDownload> {
  final ScrollController _scrollController = ScrollController();

  double _scrollPosition = 0;

  _scrollerListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
    });
  }

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    _scrollController.addListener(_scrollerListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    final arguments = ModalRoute.of(context)?.settings.arguments as Map;
    String title = arguments['title'];
    String subTitle = arguments['subTitle'];
    String images = arguments['images'];
    return Scaffold(
      persistentFooterAlignment: AlignmentDirectional.bottomCenter,
      appBar: _scrollPosition != 0
          ? AppBar(
              toolbarHeight: 50,
              backgroundColor: Colors.white,
              flexibleSpace: Padding(
                padding: const EdgeInsets.only(top: 10, left: 10),
                child: AppBarDefault(
                  name: title,
                  navigator: "/home",
                  showReset: false,
                  save: false,
                  color: Colors.black,
                ),
              ),
            )
          : AppBar(
              toolbarHeight: 240,
              backgroundColor: const Color(0xFFFCAF14),
              flexibleSpace: AppBarDefaultDis(
                mediaQuery: mediaQuery,
                images: images,
                title: title,
                subTitle: subTitle,
                navigate: '/home',
              ),
            ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        width: mediaQuery.width,
        height: mediaQuery.height,
        child: ListView.builder(
          controller: _scrollController,
          itemCount: 15,
          itemBuilder: (context, index) {
            return SizedBox(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.16,
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: SizedBox(
                      width: mediaQuery.width * 0.9,
                      height: mediaQuery.height * 0.14,
                      child: const DashedRect(
                        color: Color(0xFF1976D2),
                        strokeWidth: 2,
                        gap: 3,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: mediaQuery.width,
                    height: mediaQuery.height * 0.24,
                    child: const Padding(
                      padding: EdgeInsets.only(left: 30.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(bottom: 8.0),
                            child: Text(
                              'Exercises 1',
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 5,
                    left: 2,
                    child: Container(
                      width: 18,
                      height: 18,
                      decoration: BoxDecoration(
                        color: Colors.blue[700],
                        borderRadius:
                            const BorderRadius.all(Radius.circular(100)),
                      ),
                      child: const Icon(
                        Icons.circle,
                        size: 10,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
      persistentFooterButtons: const [
        Icon(
          Icons.arrow_downward_rounded,
          color: Colors.white,
          size: 18,
        ),
        Text(
          'DOWNLOAD',
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        )
      ],
      floatingActionButton: SizedBox(
        width: mediaQuery.width * 0.9,
        height: mediaQuery.height * 0.07,
        child: ElevatedButton(
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.blue),
          ),
          onPressed: () {},
          child: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.arrow_downward_rounded,
                color: Colors.white,
                size: 18,
              ),
              Text(
                'DOWNLOAD',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
