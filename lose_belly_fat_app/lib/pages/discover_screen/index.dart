import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lose_belly_fat_app/db/data.dart';
import 'package:lose_belly_fat_app/pages/discover_screen/components/dis_list_item.dart';
import 'package:lose_belly_fat_app/pages/discover_screen/components/dis_list_group.dart';
import 'package:lose_belly_fat_app/pages/discover_screen/components/durian_time.dart';

class DiscoverPage extends StatelessWidget {
  const DiscoverPage({super.key});

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: mediaQuery.width,
        height: mediaQuery.height,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Recent',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      onHover: (value) {},
                      child: Text(
                        'View all',
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: Colors.blue[900],
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: const EdgeInsets.only(top: 10),
                  width: mediaQuery.width,
                  height: mediaQuery.height * 0.11,
                  child: CarouselSlider.builder(
                    itemCount: 2,
                    itemBuilder: (context, index, realIndex) {
                      return Container(
                        width: mediaQuery.width,
                        height: mediaQuery.height * 0.06,
                        margin: const EdgeInsets.only(right: 5),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 5),
                        decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [
                                Color(0xFF54B0F5),
                                Color(0xFF072EF4),
                                Color(0xFF4FADF5),
                              ],
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Day 1',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18)),
                                Text('8 minutes left',
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16)),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                    options: CarouselOptions(
                      height: mediaQuery.height * 0.9,
                      autoPlay: false,
                      viewportFraction: 1,
                      initialPage: 0,
                    ),
                  ),
                ),
              ],
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Every day new',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.22,
              margin: const EdgeInsets.only(right: 5),
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Color(0xFF54B0F5),
                    Color(0xFF072EF4),
                    Color(0xFF4FADF5),
                  ],
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: mediaQuery.width * 0.8,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: mediaQuery.width * 0.4,
                          child: const Text('Killer core HIIT beginner',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20)),
                        ),
                        const Text(
                            'Want shredded abs? Hight intnsity workout mackes you feel the burn in you',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 16)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Picks for you',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
            DefaultTabController(
              length: 4,
              child: SizedBox(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.35,
                child: TabBarView(
                  children: [
                    DiscoverListItemGroup(
                      mediaQuery: mediaQuery,
                      firstTitle: "ONLY 4 moves for abs",
                      secondTitle: "Sleepy time stretching",
                      thirdTitle: "Morning warm up",
                      firstSubTitle: "8 min • Beginner",
                      secondSubTitle: "8 min",
                      thirdSubTitle: "6 min",
                      images1: 'assets/discover/images/img_1.jpg',
                      images2: 'assets/discover/images/img_2.jpg',
                      images3: 'assets/discover/images/img_3.jpg',
                    ),
                    DiscoverListItemGroup(
                      mediaQuery: mediaQuery,
                      firstTitle: "Slim down your face",
                      secondTitle: "Killer core HIIT",
                      thirdTitle: "7 min abs workout",
                      firstSubTitle: "4 min",
                      secondSubTitle: "24 min • Intermediate",
                      thirdSubTitle: "7 min • Beginner",
                      images1: 'assets/discover/images/img_4.jpg',
                      images2: 'assets/discover/images/img_5.jpg',
                      images3: 'assets/discover/images/img_6.jpg',
                    ),
                    DiscoverListItemGroup(
                      mediaQuery: mediaQuery,
                      firstTitle: "Plank challenge",
                      secondTitle: "Belly fat burner HIIT",
                      thirdTitle: "Belly fat burner HIIT",
                      firstSubTitle: "15 min • Intermediate",
                      secondSubTitle: "18 min • Intermediate",
                      thirdSubTitle: "20 min • Advanced",
                      images1: 'assets/discover/images/img_7.jpg',
                      images2: 'assets/discover/images/img_8.jpg',
                      images3: 'assets/discover/images/img_8.jpg',
                    ),
                    Column(
                      children: [
                        DiscoverListItem(
                          mediaQuery: mediaQuery,
                          title: "Butt & abs workout",
                          subTitle: "14 min . Intermediate",
                          images: 'assets/discover/images/img_9.jpg',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'For beginners',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(
              width: mediaQuery.width,
              height: 140,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: beginnerList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Container(
                      width: 140,
                      height: 140,
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8.0)),
                        gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: beginnerList[index].color,
                        ),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            bottom: 15,
                            left: 10,
                            child: Text(
                              beginnerList[index].text,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          Positioned(
                            top: 10,
                            right: 10,
                            child: SvgPicture.asset(
                              'assets/discover/svgs/${beginnerList[index].images}',
                              fit: BoxFit.cover,
                              // ignore: deprecated_member_use
                              color: Colors.white,
                              width: 50,
                              height: 50,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Beach ready',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
            DefaultTabController(
              length: 3,
              child: SizedBox(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.35,
                child: TabBarView(
                  children: [
                    DiscoverListItemGroup(
                      mediaQuery: mediaQuery,
                      firstTitle: "Bikini workout",
                      secondTitle: "Brutal ladder HIIT",
                      thirdTitle: "7 min classic",
                      firstSubTitle: "7 min • Beginner",
                      secondSubTitle: "20 min • Advanced",
                      thirdSubTitle: "7 min • Beginner",
                      images1: 'assets/discover/images/img_8.jpg',
                      images2: 'assets/discover/images/img_9.jpg',
                      images3: 'assets/discover/images/img_8.jpg',
                    ),
                    DiscoverListItemGroup(
                      mediaQuery: mediaQuery,
                      firstTitle: "Rounder booty shaping",
                      secondTitle: "Burn thigh fat",
                      thirdTitle: "Killer core  HIIT",
                      firstSubTitle: "7 min • Intermediate",
                      secondSubTitle: "14 min • Intermediate",
                      thirdSubTitle: "36 min • Advanced",
                      images1: 'assets/discover/images/img_7.jpg',
                      images2: 'assets/discover/images/img_6.jpg',
                      images3: 'assets/discover/images/img_5.jpg',
                    ),
                    Column(
                      children: [
                        DiscoverListItem(
                          mediaQuery: mediaQuery,
                          title: "Butt & abs workout",
                          subTitle: "14 min . Intermediate",
                          images: 'assets/discover/images/img_4.jpg',
                        ),
                        DiscoverListItem(
                          mediaQuery: mediaQuery,
                          title: "HIIT intermadiate",
                          subTitle: "13 min . Intermediate",
                          images: 'assets/discover/images/img_3.jpg',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Flexibility & relaxation',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(
              width: mediaQuery.width,
              height: 140,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: flexrelaxList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Container(
                      width: 140,
                      height: 140,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8.0)),
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: flexrelaxList[index].color,
                          )),
                      child: Stack(
                        children: [
                          Positioned(
                            bottom: 15,
                            left: 10,
                            child: SizedBox(
                              width: 130,
                              height: 50,
                              child: Text(
                                flexrelaxList[index].text,
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: 10,
                            right: 10,
                            child: SvgPicture.asset(
                              'assets/discover/svgs/${flexrelaxList[index].images}',
                              fit: BoxFit.cover,
                              // ignore: deprecated_member_use
                              color: Colors.white,
                              width: 50,
                              height: 50,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Fat burning',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
            DefaultTabController(
              length: 3,
              child: SizedBox(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.35,
                child: TabBarView(
                  children: [
                    DiscoverListItemGroup(
                      mediaQuery: mediaQuery,
                      firstTitle: "4 MIN Tabata",
                      secondTitle: "Fat burning HIIT",
                      thirdTitle: "Killer core HIIT",
                      firstSubTitle: "4 min • Beginner",
                      secondSubTitle: "7 min • Intermediate",
                      thirdSubTitle: "14 min • Beginner",
                      images1: 'assets/discover/images/img_2.jpg',
                      images2: 'assets/discover/images/img_3.jpg',
                      images3: 'assets/discover/images/img_4.jpg',
                    ),
                    DiscoverListItemGroup(
                      mediaQuery: mediaQuery,
                      firstTitle: "Burn 100 calories",
                      secondTitle: "7 min lose arm fat",
                      thirdTitle: "Lose fat (NO JUMPING!)",
                      firstSubTitle: "9 min • Intermediate",
                      secondSubTitle: "7 min • Beginner",
                      thirdSubTitle: "15 min • Intermediate",
                      images1: 'assets/discover/images/img_5.jpg',
                      images2: 'assets/discover/images/img_6.jpg',
                      images3: 'assets/discover/images/img_7.jpg',
                    ),
                    Column(
                      children: [
                        DiscoverListItem(
                          mediaQuery: mediaQuery,
                          title: "20 min body calorie burner",
                          subTitle: "20 min . Advanced",
                          images: 'assets/discover/images/img_8.jpg',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    'Daily Tips',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 18,
                      color: Colors.black,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    'More',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: Colors.blue,
                    ),
                  ),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: mediaQuery.width,
                  height: 180,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [
                        Color(0xFF1DCAF1),
                        Color(0xFF192EEA),
                      ],
                    ),
                  ),
                  child: Image.asset(
                    'assets/discover/images/img_8.jpg',
                    fit: BoxFit.cover,
                    scale: 1,
                    repeat: ImageRepeat.repeat,
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    'Chew Your Foot',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 18,
                      color: Colors.black,
                    ),
                  ),
                ),
                const Text(
                  'When your eat, it take at least 20 minutes for you to feel full. That\'s why we should eat more...',
                  style: TextStyle(
                    // fontWeight: FontWeight.w700,
                    fontSize: 18,
                    color: Colors.black45,
                  ),
                ),
              ],
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Body focus',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.48,
              child: GridView.count(
                crossAxisCount: 2,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pushReplacementNamed(
                        context,
                        '/disexercisedetail',
                        arguments: {
                          'title': 'Full body',
                          'subTitle':
                              'Workouts in this session engage every single muscle of your body, maximize your workout results and improve your overrall fitness.',
                          'images': 'banner1.png',
                          'bgColor': const Color(0xFFFCAF14),
                        },
                      );
                    },
                    child: Container(
                      width: 100,
                      height: 80,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [
                            Color(0xFFF8A933),
                            Color(0xFFE38220),
                          ],
                        ),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            bottom: -10,
                            right: 0,
                            child: Image.asset(
                              './assets/images/banner1.png',
                              fit: BoxFit.cover,
                              repeat: ImageRepeat.noRepeat,
                              width: 120,
                              height: 160,
                            ),
                            // SvgPicture.asset(
                            //   'assets/discover/svgs/boy.svg',
                            //   fit: BoxFit.cover,
                            //   width: 120,
                            //   height: 120,
                            // ),
                          ),
                          const Positioned(
                            bottom: 0,
                            left: 0,
                            child: SizedBox(
                              width: 130,
                              height: 50,
                              child: Text(
                                'Full body',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: 100,
                    height: 80,
                    padding: const EdgeInsets.only(left: 20),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Color(0xFFF833DB),
                          Color(0xFFDC2C87),
                        ],
                      ),
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                          bottom: -10,
                          right: 0,
                          child: Image.asset(
                            './assets/images/banner2.png',
                            fit: BoxFit.cover,
                            repeat: ImageRepeat.noRepeat,
                            width: 120,
                            height: 160,
                          ),
                        ),
                        const Positioned(
                          bottom: 0,
                          left: 0,
                          child: SizedBox(
                            width: 130,
                            height: 50,
                            child: Text(
                              'Abs',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 100,
                    height: 80,
                    padding: const EdgeInsets.only(left: 20),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Color(0xFFCA21E1),
                          Color(0xFFB1018B),
                        ],
                      ),
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                          bottom: -10,
                          right: 0,
                          child: Image.asset(
                            './assets/images/banner4.png',
                            fit: BoxFit.cover,
                            repeat: ImageRepeat.noRepeat,
                            width: 120,
                            height: 160,
                          ),
                        ),
                        const Positioned(
                          bottom: 0,
                          left: 0,
                          child: SizedBox(
                            width: 130,
                            height: 50,
                            child: Text(
                              'Arm',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 100,
                    height: 80,
                    padding: const EdgeInsets.only(left: 20),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Color(0xFF33C4F8),
                          Color(0xFF132FBE),
                        ],
                      ),
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                          bottom: -10,
                          right: 0,
                          child: Image.asset(
                            './assets/images/banner3.png',
                            fit: BoxFit.cover,
                            repeat: ImageRepeat.noRepeat,
                            width: 120,
                            height: 160,
                          ),
                        ),
                        const Positioned(
                          bottom: 0,
                          left: 0,
                          child: SizedBox(
                            width: 130,
                            height: 50,
                            child: Text(
                              'Butt & Leg',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Duration',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
            Column(
              children: [
                DurianTime(
                  mediaQuery: mediaQuery,
                  title: "Short workout",
                  images: 'assets/discover/svgs/clock.svg',
                ),
                DurianTime(
                  mediaQuery: mediaQuery,
                  title: "Long workout",
                  images: 'assets/discover/svgs/clock.svg',
                ),
              ],
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Intensity',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
            Column(
              children: [
                DurianTime(
                  mediaQuery: mediaQuery,
                  title: "Beginner",
                  images: 'assets/discover/svgs/clock.svg',
                ),
                DurianTime(
                  mediaQuery: mediaQuery,
                  title: "Intermediate",
                  images: 'assets/discover/svgs/clock.svg',
                ),
                DurianTime(
                  mediaQuery: mediaQuery,
                  title: "Advanced",
                  images: 'assets/discover/svgs/clock.svg',
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
