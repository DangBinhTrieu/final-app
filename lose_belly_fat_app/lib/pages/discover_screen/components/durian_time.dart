import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DurianTime extends StatelessWidget {
  const DurianTime({
    super.key,
    required this.mediaQuery,
    required this.title,
    required this.images,
  });

  final Size mediaQuery;
  final String title;
  final String images;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          SizedBox(
            width: 40,
            height: 40,
            child: SvgPicture.asset(
              images,
              fit: BoxFit.cover,
              width: 40,
              height: 40,
              // ignore: deprecated_member_use
              color: const Color(0xFF4D078E),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              width: mediaQuery.width * 0.68,
              height: 40,
              decoration: const BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                width: 1,
                color: Colors.black26,
              ))),
              child: Text(
                title,
                textAlign: TextAlign.left,
                style: const TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
