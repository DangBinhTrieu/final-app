import 'package:flutter/material.dart';

class DiscoverListItem extends StatelessWidget {
  const DiscoverListItem({
    super.key,
    required this.mediaQuery,
    required this.title,
    required this.subTitle,
    required this.images,
  });

  final Size mediaQuery;
  final String title;
  final String subTitle;
  final String images;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          Container(
            width: 70,
            height: 70,
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: const BorderRadius.all(Radius.circular(100)),
              image: DecorationImage(
                image: AssetImage(images),
                fit: BoxFit.cover,
                scale: 1,
              ),
            ),
          ),
          Container(
            width: mediaQuery.width * 0.68,
            height: 70,
            padding: const EdgeInsets.only(left: 20),
            decoration: const BoxDecoration(
                border: Border(
                    bottom: BorderSide(
              width: 1,
              color: Colors.black26,
            ))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: const TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                    color: Colors.black,
                  ),
                ),
                Text(
                  subTitle,
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 13,
                    color: Colors.black45,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
