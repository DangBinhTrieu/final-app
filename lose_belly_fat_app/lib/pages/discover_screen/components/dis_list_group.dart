import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/pages/discover_screen/components/dis_list_item.dart';

class DiscoverListItemGroup extends StatelessWidget {
  const DiscoverListItemGroup({
    super.key,
    required this.mediaQuery,
    required this.firstTitle,
    required this.secondTitle,
    required this.thirdTitle,
    required this.firstSubTitle,
    required this.secondSubTitle,
    required this.thirdSubTitle,
    required this.images1,
    required this.images2,
    required this.images3,
  });

  final Size mediaQuery;
  final String firstTitle,
      secondTitle,
      thirdTitle,
      firstSubTitle,
      secondSubTitle,
      thirdSubTitle,
      images1,
      images2,
      images3;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            Navigator.pushReplacementNamed(
              context,
              '/disexerdownload',
              arguments: {
                'title': 'ONLY 4 moves for abs',
                'subTitle':
                    '4 simple exercises only! Burn belly fat and firm ypur abs. Get a flat belly fast!',
                'images': 'banner1.png',
                'bgColor': const Color(0xFFFCAF14),
              },
            );
          },
          child: DiscoverListItem(
            mediaQuery: mediaQuery,
            title: firstTitle,
            subTitle: firstSubTitle,
            images: images1,
          ),
        ),
        DiscoverListItem(
          mediaQuery: mediaQuery,
          title: secondTitle,
          subTitle: secondSubTitle,
          images: images2,
        ),
        DiscoverListItem(
          mediaQuery: mediaQuery,
          title: thirdTitle,
          subTitle: thirdSubTitle,
          images: images3,
        ),
      ],
    );
  }
}
