import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lose_belly_fat_app/components/appbar_default.dart';
import 'package:lose_belly_fat_app/components/appbar_default_dis.dart';
import 'package:lose_belly_fat_app/db/data.dart';

class DiscoverExerciseDetail extends StatefulWidget {
  const DiscoverExerciseDetail({
    super.key,
  });
  @override
  State<DiscoverExerciseDetail> createState() => _DiscoverExerciseDetailState();
}

class _DiscoverExerciseDetailState extends State<DiscoverExerciseDetail> {
  final ScrollController _scrollController = ScrollController();

  double _scrollPosition = 0;

  _scrollerListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
    });
  }

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    _scrollController.addListener(_scrollerListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    final arguments = ModalRoute.of(context)?.settings.arguments as Map;
    String title = arguments['title'];
    String subTitle = arguments['subTitle'];
    String images = arguments['images'];
    return Scaffold(
      appBar: _scrollPosition != 0
          ? AppBar(
              toolbarHeight: 50,
              backgroundColor: Colors.white,
              flexibleSpace: Padding(
                padding: const EdgeInsets.only(top: 10, left: 10),
                child: AppBarDefault(
                  name: title,
                  navigator: "/home",
                  showReset: false,
                  save: false,
                  color: Colors.black,
                ),
              ),
            )
          : AppBar(
              toolbarHeight: 200,
              backgroundColor: const Color(0xFFFCAF14),
              flexibleSpace: AppBarDefaultDis(
                mediaQuery: mediaQuery,
                images: images,
                title: title,
                subTitle: subTitle,
                navigate: '/home',
              ),
            ),
      body: SizedBox(
        width: mediaQuery.width,
        height: mediaQuery.height,
        child: ListView.builder(
          controller: _scrollController,
          itemCount: disLessonExer.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                Navigator.pushReplacementNamed(
                  context,
                  '/disexerdownload',
                  arguments: {
                    'title': disLessonExer[index].name,
                    'subTitle': disLessonExer[index].des,
                    'images': disLessonExer[index].images,
                    'bgColor': disLessonExer[index].color,
                  },
                );
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: mediaQuery.width,
                  height: 70,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1,
                        color: Colors.black12,
                      ),
                    ),
                  ),
                  child: Row(
                    children: [
                      Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: disLessonExer[index]?.color,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(100)),
                        ),
                        child: SvgPicture.asset(
                          'assets/discover/svgs/${disLessonExer[index].images}',
                          color: Colors.white,
                          fit: BoxFit.contain,
                          width: 15,
                          height: 15,
                        ),
                      ),
                      Container(
                        width: mediaQuery.width * 0.7,
                        height: 70,
                        padding: const EdgeInsets.only(left: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '${disLessonExer[index].name}',
                              style: const TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                            Text(
                              '${disLessonExer[index].minus} min • ${disLessonExer[index].type}',
                              style: const TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 13,
                                color: Colors.black45,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
