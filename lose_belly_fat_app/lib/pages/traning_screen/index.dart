import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lose_belly_fat_app/db/data.dart';
import 'package:lose_belly_fat_app/db/data/fitness_goals.dart';
import 'package:lose_belly_fat_app/pages/traning_screen/components/banner_list_widget.dart';

class TraningPage extends StatefulWidget {
  const TraningPage({super.key});

  @override
  State<TraningPage> createState() => _TraningPageState();
}

class _TraningPageState extends State<TraningPage> {
  @override
  Widget build(BuildContext context) {
    List<DayExercise> arrayDay = [
      DayExercise(id: 1, netDuration: Duration(seconds: 10)),
      DayExercise(id: 2, netDuration: Duration(seconds: 10)),
      DayExercise(id: 3, netDuration: Duration(seconds: 10)),
      DayExercise(id: 4, netDuration: Duration(seconds: 10)),
      DayExercise(id: 5, netDuration: Duration(seconds: 12)),
      DayExercise(id: 6, netDuration: Duration(seconds: 11)),
      DayExercise(id: 7, netDuration: Duration(seconds: 10)),
    ];
    var mediaQuery = MediaQuery.of(context).size;
    return Material(
      child: DefaultTabController(
        length: 5,
        child: Scaffold(
          backgroundColor: Colors.black12,
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: const Text(
              'Lose Belly Fat',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 20,
                color: Colors.black,
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: IconButton(
                    onPressed: () {
                      Navigator.pushReplacementNamed(
                        context,
                        '/selectedtraining',
                      );
                    },
                    icon: const Icon(
                      Icons.description_outlined,
                      size: 20,
                      color: Colors.black38,
                    )),
              ),
              const Padding(
                padding: EdgeInsets.only(right: 15),
                child: Icon(
                  Icons.notifications,
                  size: 20,
                  color: Colors.amberAccent,
                ),
              ),
            ],
          ),
          body: TabBarView(
            children: [
              for (var i = 0; i < fitnessGoals.length; i++)
                BannerListWidget(
                  bg_color: fitnessGoals[i].color,
                  arrayList: fitnessGoals[i].dayExercises,
                  showExFirstDay: false,
                  showCirclePer: true,
                  showButton: true,
                  showThreeIcon: true,
                  mediaQuery: mediaQuery,
                  name: "${fitnessGoals[i].title}",
                  subTitle: "${fitnessGoals[i].level}",
                  showImage: true,
                  // image: SvgPicture.asset(
                  //   'assets/svgs/banner_1.svg',
                  //   fit: BoxFit.cover,
                  //   width: mediaQuery.width * 0.4,
                  //   height: mediaQuery.height * 0.22,
                  // ),
                  image: Image.asset(
                    './assets/images/banner1.png',
                    fit: BoxFit.cover,
                    repeat: ImageRepeat.noRepeat,
                    width: mediaQuery.width * 0.4,
                    height: mediaQuery.height * 0.22,
                  ),
                ),
              // BannerListWidget(
              //   bg_color: const Color(0xFF1B06D2),
              //   arrayList: arrayDay,
              //   showExFirstDay: false,
              //   showCirclePer: true,
              //   showButton: true,
              //   showThreeIcon: true,
              //   mediaQuery: mediaQuery,
              //   name: "Keep Fit",
              //   subTitle: "Intermediate",
              //   showImage: true,
              //   image: Image.asset(
              //     './assets/images/banner2.png',
              //     fit: BoxFit.cover,
              //     repeat: ImageRepeat.noRepeat,
              //     width: mediaQuery.width * 0.4,
              //     height: mediaQuery.height * 0.22,
              //   ),
              // ),
              // BannerListWidget(
              //   bg_color: const Color(0xFF660285),
              //   arrayList: arrayDay,
              //   showExFirstDay: false,
              //   showCirclePer: true,
              //   showButton: true,
              //   showThreeIcon: true,
              //   mediaQuery: mediaQuery,
              //   name: "Six Pack Abs",
              //   subTitle: "Advanced",
              //   showImage: true,
              //   image: Image.asset(
              //     './assets/images/banner2.png',
              //     fit: BoxFit.cover,
              //     repeat: ImageRepeat.noRepeat,
              //     width: mediaQuery.width * 0.4,
              //     height: mediaQuery.height * 0.22,
              //   ),
              // ),
              // BannerListWidget(
              //   bg_color: const Color(0xFFC408CE),
              //   arrayList: arrayDay,
              //   showExFirstDay: false,
              //   showCirclePer: true,
              //   showButton: true,
              //   showThreeIcon: true,
              //   mediaQuery: mediaQuery,
              //   name: "Rock Hard Abs",
              //   subTitle: "Advanced",
              //   showImage: true,
              //   image: Image.asset(
              //     './assets/images/banner4.png',
              //     fit: BoxFit.cover,
              //     repeat: ImageRepeat.noRepeat,
              //     width: mediaQuery.width * 0.4,
              //     height: mediaQuery.height * 0.22,
              //   ),
              // ),

              BannerListWidget(
                bg_color: const Color(0xFF000A0D),
                arrayList: arrayDay,
                showExFirstDay: true,
                showCirclePer: true,
                showButton: true,
                showThreeIcon: false,
                mediaQuery: mediaQuery,
                name: "My Training",
                subTitle: "",
                showImage: false,
                image: Image.asset(
                  './assets/images/banner-4.png',
                  fit: BoxFit.cover,
                  repeat: ImageRepeat.noRepeat,
                  width: mediaQuery.width * 0.4,
                  height: mediaQuery.height * 0.22,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
