import 'package:flutter/material.dart';

import 'package:lose_belly_fat_app/pages/traning_screen/components/daily_exsercise.dart';

class BannerListWidget extends StatelessWidget {
  const BannerListWidget({
    super.key,
    required this.mediaQuery,
    required this.name,
    required this.subTitle,
    required this.image,
    required this.showThreeIcon,
    required this.showButton,
    required this.showImage,
    required this.showCirclePer,
    required this.showExFirstDay,
    required this.arrayList,
    // ignore: non_constant_identifier_names
    required this.bg_color,
  });

  final Size mediaQuery;
  final String? name;
  final String? subTitle;
  final Widget image;
  final bool showThreeIcon;
  final bool showButton;
  final bool showImage;
  final bool showCirclePer;
  final bool showExFirstDay;
  final List? arrayList;
  // ignore: non_constant_identifier_names
  final Color bg_color;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: mediaQuery.width,
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Positioned(
            top: 20,
            child: Container(
              width: mediaQuery.width * 0.9,
              height: mediaQuery.height * 0.2,
              padding: const EdgeInsets.only(left: 20, top: 10, bottom: 20),
              decoration: BoxDecoration(
                  color: bg_color,
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
              child: Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            if (showThreeIcon)
                              const Row(
                                children: [
                                  Icon(
                                    Icons.bolt_rounded,
                                    size: 18,
                                    color: Colors.white,
                                  ),
                                  Icon(
                                    Icons.bolt_rounded,
                                    size: 18,
                                    color: Colors.white,
                                  ),
                                  Icon(
                                    Icons.bolt_rounded,
                                    size: 18,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              subTitle!,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        ),
                      ),
                      Text(
                        name!,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      if (showThreeIcon)
                        Container(
                          width: 110,
                          height: 30,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 5),
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                          child: const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.circle_outlined,
                                size: 12,
                                color: Colors.black87,
                              ),
                              Text(
                                '30 Days Left',
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              )
                            ],
                          ),
                        ),
                      if (!showThreeIcon)
                        InkWell(
                          onTap: () {
                            Navigator.pushReplacementNamed(
                                context, '/exsercisedailyadd');
                          },
                          child: Container(
                            width: 100,
                            height: 35,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 5, vertical: 5),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                            ),
                            child: const Center(
                              child: Text(
                                '+ADD',
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 190,
            child: SizedBox(
              width: mediaQuery.width * 0.9,
              height: mediaQuery.height * 0.8,
              child: ListView(
                scrollDirection: Axis.vertical,
                children: [
                  if (!showExFirstDay)
                    SizedBox(
                      width: mediaQuery.width * 0.9,
                      height: mediaQuery.height * 0.18,
                      child: InkWell(
                        onTap: () {
                          Navigator.pushReplacementNamed(
                            context,
                            '/daily-exercises',
                          );
                        },
                        child: DailyExercises(
                          mediaQuery: mediaQuery,
                          showButton: showButton,
                          showCirclePer: showCirclePer,
                          showExFirstDay: showExFirstDay,
                          title: 1,
                          subTitle: Duration(seconds: 10),
                        ),
                      ),
                    ),
                  Container(
                    width: mediaQuery.width * 0.9,
                    height: mediaQuery.height * 0.56,
                    padding: const EdgeInsets.only(bottom: 20),
                    child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: arrayList!.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.pushReplacementNamed(
                              context,
                              '/daily-exercises',
                            );
                          },
                          child: DailyExercises(
                            mediaQuery: mediaQuery,
                            showButton: !showButton,
                            showCirclePer: !showCirclePer,
                            showExFirstDay: showExFirstDay,
                            title: arrayList![index].id,
                            subTitle: arrayList![index].netDuration,
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (showImage)
            Positioned(
              right: 0,
              top: 12,
              child: image,
            ),
        ],
      ),
    );
  }
}
