import 'package:flutter/material.dart';

class SelectedTraining extends StatelessWidget {
  const SelectedTraining({super.key});

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      // backgroundColor: const Color(0xFF030369),
      appBar: AppBar(
        // backgroundColor: const Color(0xFF030369),
        title: IconButton(
          onPressed: () {
            Navigator.pushReplacementNamed(
              context,
              '/home',
            );
          },
          icon: const Icon(
            Icons.close,
            size: 20,
            color: Colors.black45,
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        width: mediaQuery.width,
        height: mediaQuery.height,
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            SizedBox(
              width: mediaQuery.width * 0.6,
              child: const Text(
                'Select Training Plan',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.16,
              decoration: BoxDecoration(
                color: Colors.blue,
                border: Border.all(width: 2, color: Colors.yellow),
                borderRadius: const BorderRadius.all(Radius.circular(10)),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 20,
                    left: 30,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        Text(
                          'Lose Belly Fat',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 5,
                    child: Image.asset(
                      './assets/images/banner2.png',
                      fit: BoxFit.cover,
                      repeat: ImageRepeat.noRepeat,
                      width: 130,
                      height: 130,
                    ),
                    // SvgPicture.asset(
                    //   'assets/svgs/boy.svg',
                    //   semanticsLabel: 'Image Splah SVG',
                    //   fit: BoxFit.cover,
                    //   width: 130,
                    //   height: 130,
                    // ),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: Container(
                      width: 100,
                      height: 30,
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      decoration: const BoxDecoration(
                        color: Colors.yellow,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.pushReplacementNamed(context, '/home');
                            },
                            child: const Icon(
                              Icons.check,
                              size: 18,
                              color: Colors.black,
                            ),
                          ),
                          const Text(
                            'Selected',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.16,
              decoration: const BoxDecoration(
                color: Color(0xFF1C29BB),
                // border: Border.all(width: 2, color: Colors.yellow),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 20,
                    left: 30,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        Text(
                          'Keep Fit',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 5,
                    child: Image.asset(
                      './assets/images/banner1.png',
                      fit: BoxFit.cover,
                      repeat: ImageRepeat.noRepeat,
                      width: 130,
                      height: 130,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.16,
              decoration: const BoxDecoration(
                color: Color(0xFF500363),
                // border: Border.all(width: 2, color: Colors.yellow),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 20,
                    left: 30,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        Text(
                          'Six Pack Abs',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 5,
                    child: Image.asset(
                      './assets/images/banner3.png',
                      fit: BoxFit.cover,
                      repeat: ImageRepeat.noRepeat,
                      width: 130,
                      height: 130,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              width: mediaQuery.width,
              height: mediaQuery.height * 0.16,
              decoration: const BoxDecoration(
                color: Color(0xFF8E096B),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Stack(
                children: [
                  const Positioned(
                    top: 20,
                    left: 30,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.bolt_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        Text(
                          'Rock Hard Abs',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 5,
                    child: Image.asset(
                      './assets/images/banner4.png',
                      fit: BoxFit.cover,
                      repeat: ImageRepeat.noRepeat,
                      width: 130,
                      height: 130,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            SizedBox(
              width: mediaQuery.width * 0.6,
              child: const Text(
                'More',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                width: mediaQuery.width * 0.8,
                height: mediaQuery.height * 0.16,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      Color(0xFF5806B6),
                      Color(0xFF8911CF),
                    ],
                  ),
                ),
                child: Stack(
                  children: [
                    const Positioned(
                      top: 20,
                      left: 30,
                      child: Text(
                        'Data Transfer - Coppy Data',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      child: Container(
                        width: 40,
                        height: 20,
                        decoration: const BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        child: const Center(
                          child: Text(
                            'AD',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      bottom: 0,
                      child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.arrow_forward_ios,
                            size: 12,
                            color: Colors.white,
                          )),
                    ),
                    const Positioned(
                      left: 15,
                      bottom: 10,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.star,
                            size: 12,
                            color: Colors.yellow,
                          ),
                          Text(
                            ' 4.8',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}
