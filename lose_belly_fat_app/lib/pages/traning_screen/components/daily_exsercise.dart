import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/components/start_button.dart';

class DailyExercises extends StatelessWidget {
  const DailyExercises({
    super.key,
    required this.mediaQuery,
    required this.showButton,
    required this.showCirclePer,
    required this.showExFirstDay,
    required this.title,
    required this.subTitle,
  });

  final Size mediaQuery;
  final bool showButton;
  final bool showCirclePer;
  final bool showExFirstDay;
  final int title;
  final Duration subTitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: mediaQuery.width * 0.8,
      height: showCirclePer
          ? mediaQuery.height * 0.18
          : mediaQuery.height * 0.11, //0.11
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Day $title',
                      style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 18)),
                  Text(
                    '${subTitle.inSeconds.toString().padLeft(2, "0")} minutes',
                    style: const TextStyle(
                        color: Colors.black87,
                        fontWeight: FontWeight.w500,
                        fontSize: 16),
                  ),
                ],
              ),
              if (showCirclePer)
                Container(
                  width: 40,
                  height: 40,
                  decoration: const BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                  ),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 30,
                        height: 30,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                        ),
                        child: const Center(
                          child: Text(
                            '16%',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              if (showExFirstDay)
                TextButton(
                  onPressed: () {},
                  child: const Text(
                    "...",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black54,
                        fontWeight: FontWeight.w600),
                  ),
                )
            ],
          ),
          if (showButton)
            StartButtonWidget(
              mediaQuery: mediaQuery.height * 0.05,
              name: "START",
              bgColor: const Color(0xFF1976D2),
              txtColor: const Color(0xFFFFFFFF),
              navigatorFunction: () {},
            ),
        ],
      ),
    );
  }
}
