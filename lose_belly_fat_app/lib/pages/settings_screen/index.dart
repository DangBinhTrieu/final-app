import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/pages/settings_screen/component/option_choose.dart';
import 'package:lose_belly_fat_app/pages/settings_screen/component/show_modal_bottom_sheet.dart';
import 'package:lose_belly_fat_app/pages/settings_screen/component/work_out_data.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    List<WorkOut> data = const [
      WorkOut(
        Icon(
          Icons.music_note_rounded,
          color: Colors.black38,
        ),
        'Sound Options',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.sentiment_dissatisfied,
          color: Colors.black38,
        ),
        'Coach voice',
        'Thomas',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.sentiment_dissatisfied,
          color: Colors.black38,
        ),
        'Traning rest',
        '34 secs',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
    ];
    List<WorkOut> data2 = const [
      WorkOut(
        Icon(
          Icons.favorite_border,
          color: Colors.black38,
        ),
        'Sync to Google Fit',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.favorite_border,
          color: Colors.black38,
        ),
        'Health Data',
        'Thomas',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.notifications_none,
          color: Colors.black38,
        ),
        'Remind me to work out every day',
        '34 secs',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.description_rounded,
          color: Colors.black38,
        ),
        'First day of week',
        'sunday',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.notifications_none,
          color: Colors.black38,
        ),
        'Metric & Imperial Units',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.hdr_auto_outlined,
          color: Colors.black38,
        ),
        'Language Options',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.refresh,
          color: Colors.black38,
        ),
        'Restart progress',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.delete,
          color: Colors.black38,
        ),
        'Delete all data',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.clean_hands,
          color: Colors.black38,
        ),
        'Clean downloaded resources',
        '34 secs',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
    ];
    List<WorkOut> data3 = const [
      WorkOut(
        Icon(
          Icons.grade_outlined,
          color: Colors.black38,
        ),
        'Rate us',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.share,
          color: Colors.black38,
        ),
        'Share with Friends',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.sentiment_dissatisfied,
          color: Colors.black38,
        ),
        'Common question',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
      WorkOut(
        Icon(
          Icons.description_outlined,
          color: Colors.black38,
        ),
        'Privacy policy',
        '',
        Icon(
          Icons.arrow_forward_ios,
          size: 14,
          color: Colors.black26,
        ),
      ),
    ];

    return Scaffold(
      backgroundColor: const Color(0xFFF7F7F7),
      appBar: AppBar(
        title: const Text(
          'Settings',
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 18,
            color: Colors.black,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: InkWell(
              onTap: () {},
              child: Text(
                'FEEDBACK',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                  color: Colors.pink[400],
                ),
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: SizedBox(
          width: mediaQuery.width,
          height: mediaQuery.height,
          child: ListView(
            // physics: NeverScrollableScrollPhysics(),
            shrinkWrap: false,
            children: [
              Container(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.08,
                decoration: const BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            style: BorderStyle.solid,
                            width: 1,
                            color: Colors.black12))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              'Backup & Restore',
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 18,
                                color: Colors.black,
                              ),
                            ),
                            Icon(Icons.apple)
                          ],
                        ),
                        Text(
                          'Sign in and synchronize your data',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Colors.black87,
                          ),
                        )
                      ],
                    ),
                    Icon(
                      Icons.autorenew,
                      size: 24,
                      color: Colors.pink[400],
                    )
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  'WORKOUT',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Colors.black38,
                  ),
                ),
              ),
              SizedBox(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.25,
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    return OptionChoose(
                      mediaQuery: mediaQuery,
                      icon: data[index].icon,
                      titleOption: data[index].titleOption,
                      subTitleOption: data[index].subTitleOption,
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: SizedBox(
                  width: mediaQuery.width,
                  height: mediaQuery.height * 0.08,
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.sentiment_dissatisfied,
                              color: Colors.black38,
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Remove ads",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),
                              Text(
                                "Upgrade to remove ads",
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color: Colors.black87,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        const MaterialStatePropertyAll(Color(0xFFE8E8E8)),
                    elevation: const MaterialStatePropertyAll(1),
                    padding: const MaterialStatePropertyAll(
                        EdgeInsets.symmetric(vertical: 16)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        side: BorderSide(
                          style: BorderStyle.none,
                        ),
                      ),
                    ),
                  ),
                  onPressed: () => showModalBottomSheet(
                        isScrollControlled: true,
                        isDismissible: true,
                        // showDragHandle: true,
                        enableDrag: false,
                        elevation: 1,
                        context: context,
                        builder: (context) {
                          return ShowModalBottomSheet(mediaQuery: mediaQuery);
                        },
                      ),
                  child: const Text(
                    "Why I see ads",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontFamily: AutofillHints.birthday,
                      color: Colors.pink,
                    ),
                  )),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: Text(
                  'GENERAL SETTINGS',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Colors.black38,
                  ),
                ),
              ),
              Container(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.72,
                margin: const EdgeInsets.symmetric(vertical: 15.0),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 1,
                      style: BorderStyle.solid,
                      color: Colors.black12,
                    ),
                  ),
                ),
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: data2.length,
                  itemBuilder: (context, index) {
                    return OptionChoose(
                      mediaQuery: mediaQuery,
                      icon: data2[index].icon,
                      titleOption: data2[index].titleOption,
                      subTitleOption: data2[index].subTitleOption,
                    );
                  },
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: Text(
                  'SUPPORT US',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Colors.black38,
                  ),
                ),
              ),
              Container(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.32,
                margin: const EdgeInsets.symmetric(vertical: 15.0),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 1,
                      style: BorderStyle.solid,
                      color: Colors.black12,
                    ),
                  ),
                ),
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: data3.length,
                  itemBuilder: (context, index) {
                    return OptionChoose(
                      mediaQuery: mediaQuery,
                      icon: data3[index].icon,
                      titleOption: data3[index].titleOption,
                      subTitleOption: data3[index].subTitleOption,
                    );
                  },
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: Text(
                  'Version 1.5.9C',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                    color: Colors.black87,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
