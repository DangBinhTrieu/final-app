import 'package:flutter/cupertino.dart';

class WorkOut {
  final Widget icon;
  final String titleOption;
  final String subTitleOption;
  final Widget? iconArrow;

  const WorkOut(
      this.icon, this.titleOption, this.subTitleOption, this.iconArrow);
}
