import 'package:flutter/material.dart';

class OptionChoose extends StatefulWidget {
  const OptionChoose({
    super.key,
    required this.mediaQuery,
    required this.icon,
    required this.titleOption,
    required this.subTitleOption,
  });

  final Size mediaQuery;
  final Widget icon;
  final String titleOption;
  final String subTitleOption;
  // final Widget iconArrow;

  @override
  State<OptionChoose> createState() => _OptionChooseState();
}

class _OptionChooseState extends State<OptionChoose> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10),
      child: Container(
        width: widget.mediaQuery.width,
        height: widget.mediaQuery.height * 0.08,
        decoration: const BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    style: BorderStyle.solid,
                    width: 1,
                    color: Colors.black12))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: widget.icon,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: widget.subTitleOption != ''
                      ? CrossAxisAlignment.start
                      : CrossAxisAlignment.center,
                  children: [
                    Text(
                      widget.titleOption,
                      style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                    if (widget.subTitleOption != '')
                      Text(
                        widget.subTitleOption,
                        style: const TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: Colors.black87,
                        ),
                      )
                  ],
                ),
              ],
            ),
            InkWell(
              onTap: () {},
              child: const Icon(
                Icons.arrow_forward_ios,
                size: 14,
                color: Colors.black26,
              ),
            )
          ],
        ),
      ),
    );
  }
}
