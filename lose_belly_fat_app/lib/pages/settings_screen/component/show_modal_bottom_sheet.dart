import 'package:flutter/material.dart';

class ShowModalBottomSheet extends StatelessWidget {
  const ShowModalBottomSheet({
    super.key,
    required this.mediaQuery,
  });

  final Size mediaQuery;

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      heightFactor: 0.8,
      child: Container(
        width: mediaQuery.width,
        height: mediaQuery.height,
        padding: const EdgeInsets.all(20),
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0))),
        child: Stack(
          alignment: Alignment.center,
          children: [
            const Positioned(
              top: 10,
              right: 10,
              child: Icon(
                Icons.close,
                size: 28,
                color: Colors.black38,
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: mediaQuery.width,
                  height: mediaQuery.height * 0.2,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 100,
                        height: 100,
                        decoration: const BoxDecoration(
                          color: Colors.pink,
                          borderRadius: BorderRadius.all(
                            Radius.circular(100),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 40,
                        child: ElevatedButton(
                          onPressed: () {},
                          style: ButtonStyle(
                            backgroundColor: const MaterialStatePropertyAll(
                                Color(0xFFFFFFFF)),
                            elevation: const MaterialStatePropertyAll(3),
                            padding: const MaterialStatePropertyAll(
                                EdgeInsets.symmetric(
                                    vertical: 16, horizontal: 20)),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                side: BorderSide(
                                  style: BorderStyle.none,
                                ),
                              ),
                            ),
                          ),
                          child: const Row(
                            children: [
                              Icon(
                                Icons.favorite_border,
                                size: 20,
                                color: Colors.pink,
                              ),
                              Text("FOR FREE"),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: Text(
                    "Con't want ads?",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.w700,
                      fontFamily: AutofillHints.birthday,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  width: mediaQuery.width * 0.8,
                  height: mediaQuery.height * 0.14,
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                    width: 1,
                    style: BorderStyle.solid,
                    color: Colors.black12,
                  ))),
                  child: const Text(
                    "Our app is free, so we need to make money by displaying ads. But we won't pop up any ads outside the app.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: AutofillHints.birthday,
                      color: Colors.black54,
                    ),
                  ),
                ),
                Container(
                  width: mediaQuery.width * 0.8,
                  height: mediaQuery.height * 0.14,
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: const Text(
                    "We're trying to find ways to display ads without disturbing you. Please feel free to contact us if you have any suddestions.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: AutofillHints.birthday,
                      color: Colors.black38,
                    ),
                  ),
                ),
                Container(
                  width: mediaQuery.width * 0.8,
                  height: mediaQuery.height * 0.14,
                  padding: const EdgeInsets.only(bottom: 10),
                  child: const Text(
                    "We also offer the function of removing ads, you could remove adsif you want to, Thanks for your understanding.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: AutofillHints.birthday,
                      color: Colors.black38,
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 10,
              child: ElevatedButton(
                onPressed: () {},
                style: const ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(Colors.pink),
                  textStyle: MaterialStatePropertyAll(
                    TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      fontFamily: AutofillHints.countryName,
                    ),
                  ),
                ),
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                  child: Text("GOT IT"),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
