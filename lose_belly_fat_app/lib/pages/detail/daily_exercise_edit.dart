import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/components/appbar_default.dart';
import 'package:lose_belly_fat_app/components/exercise_list_item.dart';
import 'package:lose_belly_fat_app/db/data.dart';

import '../../db/model.dart';

class DailyExcercisesDetailEdit extends StatefulWidget {
  const DailyExcercisesDetailEdit({super.key});

  @override
  State<DailyExcercisesDetailEdit> createState() =>
      _DailyExcercisesDetailEditState();
}

class _DailyExcercisesDetailEditState extends State<DailyExcercisesDetailEdit> {
  void updateExerciseItemDate(int oldIndex, int newIndex) {
    setState(() {
      if (oldIndex < newIndex) {
        newIndex--;
      }
    });
    final Exercise item = exercise_day_one.removeAt(oldIndex);
    exercise_day_one.insert(newIndex, item);
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: const Color(0xFFF9F9F9),
      appBar: AppBar(
        title: const AppBarDefault(
          name: "Edit",
          navigator: "/daily-exercises",
          showReset: true,
          save: false,
          color: Colors.black,
        ),
      ),
      body: Container(
        width: mediaQuery.width,
        height: mediaQuery.height,
        padding: const EdgeInsets.all(8.0),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(
              width: 1,
              color: Colors.black12,
            ),
          ),
        ),
        child: ReorderableListView(
          reverse: true,
          buildDefaultDragHandles: false,
          children: [
            for (int index = 0; index < exercise_list.length; index++)
              ReorderableDragStartListener(
                index: index,
                key: Key('$index'),
                child: InkWell(
                  onTap: () {
                    Navigator.pushReplacementNamed(
                      context,
                      '/exerciseitemdetail',
                      arguments: {
                        'navigate': '/daily-exseercises-edit',
                        'replacebutton': false,
                        'addbutton': false,
                        'exerciseFother': 'exercise',
                        'exerciseItem': exercise_list[index],
                        'exercise': null,
                      },
                    );
                  },
                  child: ExerciseListItem(
                    mediaQuery: mediaQuery,
                    showReplace: false,
                    newExercise: false,
                    item: exercise_list[index],
                    imageGif: '',
                  ),
                ),
              ),
          ],
          onReorder: (oldIndex, newIndex) =>
              updateExerciseItemDate(oldIndex, newIndex),
        ),
      ),
    );
  }
}
