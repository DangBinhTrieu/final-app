import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/db/model.dart';
import 'package:timer_count_down/timer_controller.dart';
import 'package:timer_count_down/timer_count_down.dart';

class ExercisePracticeStart extends StatefulWidget {
  const ExercisePracticeStart({super.key});
  @override
  State<ExercisePracticeStart> createState() => _ExercisePracticeStartState();
}

class _ExercisePracticeStartState extends State<ExercisePracticeStart> {
  final CountdownController? controller = CountdownController(autoStart: true);
  bool pauseCheck = true;
  int increase = 0;
  static const maxSeconds = 10;
  int second = maxSeconds;

  Timer? timer;
  @override
  void initState() {
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (second > 0) {
          second--;
        } else {
          timer.cancel();
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    final arguments = ModalRoute.of(context)?.settings.arguments as Map;
    List<Exercise> exerciseList = arguments['exerciseList'];
    String exerciseFother = arguments['exerciseFother'];

    return Material(
      child: Scaffold(
        body: SizedBox(
          width: mediaQuery.width,
          height: mediaQuery.height,
          child: Column(
            children: [
              Expanded(
                flex: 6,
                child: Container(
                  padding: const EdgeInsets.all(20),
                  width: mediaQuery.width,
                  height: mediaQuery.height * 0.6,
                  decoration: const BoxDecoration(
                    color: Colors.black12,
                  ),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      SizedBox(
                        width: mediaQuery.width,
                        height: mediaQuery.height * 0.6,
                        child: Image.asset(
                          'assets/gifs/$exerciseFother/${exerciseList[increase].animation}',
                          scale: 1,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Positioned(
                        left: 0,
                        top: 0,
                        child: IconButton(
                          onPressed: () {
                            Navigator.pushReplacementNamed(
                                context, '/daily-exercises');
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            color: Colors.black54,
                            size: 24,
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: Row(
                          children: [
                            IconButton(
                                style: const ButtonStyle(
                                    backgroundColor: MaterialStatePropertyAll(
                                        Colors.black26),
                                    side: MaterialStatePropertyAll(BorderSide(
                                        width: 1, color: Colors.black26))),
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.back_hand_rounded,
                                  color: Colors.white,
                                  size: 16,
                                )),
                            const SizedBox(
                              width: 10,
                            ),
                            IconButton(
                                onPressed: () {},
                                style: const ButtonStyle(
                                    backgroundColor: MaterialStatePropertyAll(
                                        Colors.black26),
                                    side: MaterialStatePropertyAll(BorderSide(
                                        width: 1, color: Colors.black26))),
                                icon: const Icon(
                                  Icons.back_hand_rounded,
                                  color: Colors.white,
                                  size: 16,
                                )),
                          ],
                        ),
                      ),
                      Positioned(
                        top: 0,
                        right: 0,
                        child: Column(
                          children: [
                            IconButton(
                                style: const ButtonStyle(
                                    backgroundColor: MaterialStatePropertyAll(
                                        Colors.black26),
                                    side: MaterialStatePropertyAll(BorderSide(
                                        width: 1, color: Colors.black26))),
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.back_hand_rounded,
                                  color: Colors.white,
                                  size: 16,
                                )),
                            const SizedBox(
                              height: 10,
                            ),
                            IconButton(
                                onPressed: () {},
                                style: const ButtonStyle(
                                    backgroundColor: MaterialStatePropertyAll(
                                        Colors.black26),
                                    side: MaterialStatePropertyAll(BorderSide(
                                        width: 1, color: Colors.black26))),
                                icon: const Icon(
                                  Icons.video_call_rounded,
                                  color: Colors.white,
                                  size: 16,
                                )),
                            const SizedBox(
                              height: 10,
                            ),
                            IconButton(
                              onPressed: () {},
                              style: const ButtonStyle(
                                  backgroundColor:
                                      MaterialStatePropertyAll(Colors.black26),
                                  side: MaterialStatePropertyAll(BorderSide(
                                      width: 1, color: Colors.black26))),
                              icon: const Icon(
                                Icons.back_hand_rounded,
                                color: Colors.white,
                                size: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 4,
                child: SizedBox(
                  width: mediaQuery.width,
                  height: mediaQuery.height * 0.3,
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 50),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            if (second != 0)
                              const Text(
                                "READY TO GO",
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 35,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: AutofillHints.addressCity),
                              ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 15),
                                  child: SizedBox(
                                    width: mediaQuery.width * 0.4,
                                    height: mediaQuery.height * 0.1,
                                    child: Text(
                                      exerciseList[increase].name,
                                      textAlign: TextAlign.center,
                                      overflow: TextOverflow.fade,
                                      style: const TextStyle(
                                          color: Colors.black,
                                          fontSize: 24,
                                          fontWeight: FontWeight.w700,
                                          fontFamily:
                                              AutofillHints.addressCity),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                      color: Colors.black54,
                                    ),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(100)),
                                  ),
                                  child: Center(
                                    child: InkWell(
                                      onTap: () {
                                        // Navigator.pushReplacementNamed(
                                        //   context,
                                        //   '/exerciseitemdetail',
                                        // );
                                      },
                                      child: const Text(
                                        '?',
                                        style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w700,
                                            fontFamily:
                                                AutofillHints.addressCity),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            if (second == 0)
                              Countdown(
                                seconds:
                                    int.parse(exerciseList[increase].repeat),
                                build: (p0, p1) => Text(
                                  '00:${p1.toString()}',
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontSize: 40,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: AutofillHints.addressCity),
                                ),
                                interval: const Duration(seconds: 1),
                                onFinished: () {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      elevation: 1,
                                      backgroundColor: Colors.blue,
                                      content: Text('Timer is done!'),
                                    ),
                                  );
                                },
                              ),
                            // const Text(
                            //   "00:32",
                            //   style: TextStyle(
                            //       color: Colors.black,
                            //       fontSize: 40,
                            //       fontWeight: FontWeight.w700,
                            //       fontFamily: AutofillHints.addressCity),
                            // ),
                          ],
                        ),
                      ),
                      Positioned(
                        bottom: 30,
                        child: SizedBox(
                          width: mediaQuery.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              if (increase > 1)
                                IconButton(
                                  onPressed: () {
                                    setState(
                                      () {
                                        increase--;
                                      },
                                    );
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.black54,
                                    size: 30,
                                  ),
                                ),
                              if (increase <= 1)
                                const SizedBox(
                                  width: 50,
                                ),
                              if (second != 0)
                                Container(
                                  width: 80,
                                  height: 80,
                                  decoration: const BoxDecoration(
                                    color: Colors.black12,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100)),
                                  ),
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Center(
                                        child: Transform.scale(
                                          scale: 2,
                                          child: CircularProgressIndicator(
                                            backgroundColor: Colors.white,
                                            value: second / maxSeconds,
                                            valueColor:
                                                const AlwaysStoppedAnimation(
                                                    Colors.blue),
                                            strokeWidth: 4,
                                            color: Colors.blue,
                                            strokeCap: StrokeCap.round,
                                          ),
                                        ),
                                      ),
                                      Text(
                                        '$second',
                                        style: const TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.blue,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              if (second == 0)
                                IconButton(
                                  style: const ButtonStyle(
                                      backgroundColor:
                                          MaterialStatePropertyAll(Colors.blue),
                                      side: MaterialStatePropertyAll(BorderSide(
                                          width: 1, color: Colors.blue))),
                                  onPressed: () {
                                    setState(() {
                                      pauseCheck = !pauseCheck;
                                    });
                                    showDialog(
                                      barrierDismissible: false,
                                      useSafeArea: false,
                                      useRootNavigator: false,
                                      context: context,
                                      builder: (context) {
                                        return ShowDIalogPause(
                                          mediaQuery: mediaQuery,
                                          pauseCheck: pauseCheck,
                                        );
                                      },
                                    );
                                  },
                                  icon: Icon(
                                    // ignore: dead_code
                                    pauseCheck ? Icons.pause : Icons.play_arrow,
                                    color: Colors.white,
                                    size: 60,
                                  ),
                                ),
                              if (increase <= exerciseList.length - 1)
                                IconButton(
                                  onPressed: () {
                                    setState(() {
                                      increase++;
                                    });
                                  },
                                  icon: const Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.black54,
                                    size: 30,
                                  ),
                                ),
                              if (increase >= exerciseList.length - 1)
                                const SizedBox(
                                  width: 50,
                                ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class ShowDIalogPause extends StatefulWidget {
  ShowDIalogPause({
    super.key,
    required this.mediaQuery,
    required this.pauseCheck,
  });

  final Size mediaQuery;
  late bool pauseCheck;

  @override
  State<ShowDIalogPause> createState() => _ShowDIalogPauseState();
}

class _ShowDIalogPauseState extends State<ShowDIalogPause> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.mediaQuery.width,
      height: widget.mediaQuery.height,
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      decoration: const BoxDecoration(color: Color(0x3E000000)),
      child: Stack(
        alignment: Alignment.centerLeft,
        children: [
          Positioned(
            left: 0,
            top: 10,
            child: IconButton(
              onPressed: () {
                // Navigator.pushReplacementNamed(
                //     context,
                //     '/daily-exercises');
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.white,
                size: 24,
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Pause',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  decoration: TextDecoration.none,
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              Container(
                width: widget.mediaQuery.width,
                height: widget.mediaQuery.height * 0.08,
                decoration: const BoxDecoration(
                  color: Color(0xAE171731),
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text(
                    'Restart this exercise',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      decoration: TextDecoration.none,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                width: widget.mediaQuery.width,
                height: widget.mediaQuery.height * 0.08,
                decoration: const BoxDecoration(
                  color: Color(0xAE171731),
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: TextButton(
                  onPressed: () {},
                  child: const Text(
                    'Quit',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      decoration: TextDecoration.none,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                width: widget.mediaQuery.width,
                height: widget.mediaQuery.height * 0.08,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    setState(() {
                      widget.pauseCheck = !widget.pauseCheck;
                    });
                  },
                  child: const Text(
                    'Resume',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      decoration: TextDecoration.none,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            child: SizedBox(
              width: widget.mediaQuery.width,
              child: TextButton(
                onPressed: () {},
                child: const Text(
                  'Feedback',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    decorationColor: Colors.white,
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
