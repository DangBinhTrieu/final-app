import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/components/appbar_default.dart';
import 'package:lose_belly_fat_app/components/exercise_list_item.dart';
import 'package:lose_belly_fat_app/db/data.dart';
import 'package:lose_belly_fat_app/db/model.dart';

class ExserciseItemReplace extends StatelessWidget {
  const ExserciseItemReplace({super.key});

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;

    final arguments = ModalRoute.of(context)?.settings.arguments as Map;
    Exercise? exerciseItem = arguments['exerciseItem'];
    return Scaffold(
      backgroundColor: const Color(0xFFF9F9F9),
      appBar: AppBar(
        title: const AppBarDefault(
          name: "Replace Exercise",
          navigator: "/daily-exseercises-edit",
          showReset: false,
          save: false,
          color: Colors.black,
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 10, bottom: 10, top: 8),
            child: Text(
              'Current',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 18,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: exerciseItem != null
                ? ExerciseListItem(
                    mediaQuery: mediaQuery,
                    showReplace: true,
                    newExercise: false,
                    item: exerciseItem,
                    imageGif: '')
                : ExerciseListItem(
                    mediaQuery: mediaQuery,
                    showReplace: true,
                    newExercise: false,
                    item: null,
                    imageGif: ''),
          ),
          const Padding(
            padding: EdgeInsets.only(left: 10, bottom: 20, top: 8),
            child: Text(
              'Replace with',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 18,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            width: mediaQuery.width,
            height: mediaQuery.height * 0.66,
            padding: const EdgeInsets.all(8.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Colors.black12,
                ),
              ),
            ),
            child: exerciseItem != null
                ? ListView.builder(
                    key: UniqueKey(),
                    itemCount: exercise_list.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.pushReplacementNamed(
                            context,
                            '/exerciseitemdetail',
                            arguments: {
                              'navigate': '/exerciseitemreplace',
                              'replacebutton': true,
                              'addbutton': false,
                              'exerciseFother': 'exercise',
                              'exerciseItem': exerciseItem,
                              'exercise': null,
                            },
                          );
                        },
                        child: ExerciseListItem(
                            mediaQuery: mediaQuery,
                            showReplace: true,
                            newExercise: false,
                            item: exerciseItem,
                            imageGif: ''),
                      );
                    },
                  )
                : ListView.builder(
                    key: UniqueKey(),
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return ExerciseListItem(
                          mediaQuery: mediaQuery,
                          showReplace: true,
                          newExercise: false,
                          item: null,
                          imageGif: '');
                    },
                  ),
          ),
        ],
      ),
    );
  }
}
