import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/components/appbar_default.dart';
import 'package:lose_belly_fat_app/components/appbar_scroll_after.dart';
import 'package:lose_belly_fat_app/components/dot_border/dashed_rect.dart';
import 'package:lose_belly_fat_app/db/data.dart';

class DailyExercisesDetail extends StatefulWidget {
  const DailyExercisesDetail({super.key});

  @override
  State<DailyExercisesDetail> createState() => _DailyExercisesDetailState();
}

class _DailyExercisesDetailState extends State<DailyExercisesDetail> {
  final ScrollController _scrollController = ScrollController();

  double _scrollPosition = 0;

  _scrollerListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
    });
  }

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    _scrollController.addListener(_scrollerListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.blue[700],
      appBar: _scrollPosition != 0
          ? AppBar(
              toolbarHeight: 50,
              backgroundColor: Colors.white,
              flexibleSpace: const Padding(
                padding: EdgeInsets.only(top: 10, left: 10),
                child: AppBarDefault(
                  name: "Day 1",
                  navigator: "/daily-exercises",
                  showReset: false,
                  save: false,
                  color: Colors.black,
                ),
              ),
            )
          : AppBar(
              toolbarHeight: 300,
              backgroundColor: Colors.blue[700],
              flexibleSpace: AppBarScrollAfter(mediaQuery: mediaQuery),
            ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: mediaQuery.width,
          height: mediaQuery.height,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
          child: Stack(
            alignment: Alignment.topCenter,
            children: [
              Positioned(
                top: 0,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: SizedBox(
                    width: mediaQuery.width * 0.9,
                    height: mediaQuery.height * 0.04,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text(
                          "Exercises(18)",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.black45,
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushReplacementNamed(
                                context, '/daily-exseercises-edit');
                          },
                          child: const Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Edit",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14,
                                  color: Colors.black45,
                                ),
                              ),
                              Icon(
                                Icons.edit,
                                size: 12,
                                color: Colors.black45,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: mediaQuery.height * 0.08,
                child: SizedBox(
                  width: mediaQuery.width * 0.9,
                  height: mediaQuery.height * 0.8,
                  // color: Colors.redAccent,
                  child: ListView.builder(
                    controller: _scrollController,
                    itemCount: exercise_day_one.length,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        width: mediaQuery.width,
                        height: mediaQuery.height * 0.26,
                        child: Stack(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: SizedBox(
                                width: mediaQuery.width * 0.9,
                                height: mediaQuery.height * 0.24,
                                child: const DashedRect(
                                  color: Color(0xFF1976D2),
                                  strokeWidth: 2,
                                  gap: 3,
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.pushReplacementNamed(
                                  context,
                                  '/exerciseitemdetail',
                                  arguments: {
                                    'navigate': '/daily-exercises',
                                    'replacebutton': false,
                                    'addbutton': false,
                                    'exerciseFother': 'exercise',
                                    'exerciseItem': exercise_day_one[index],
                                    'exercise': exercise_day_one,
                                  },
                                );
                              },
                              child: SizedBox(
                                width: mediaQuery.width * 0.9,
                                height: mediaQuery.height * 0.24,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 30.0),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 8.0),
                                        child: Text(
                                          exercise_day_one[index].name,
                                          style: const TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: mediaQuery.width * 0.9,
                                        height: mediaQuery.height * 0.18,
                                        decoration: const BoxDecoration(
                                          border: Border.symmetric(
                                              vertical: BorderSide(
                                                width: 1,
                                                color: Colors.black38,
                                                style: BorderStyle.solid,
                                              ),
                                              horizontal: BorderSide(
                                                width: 1,
                                                color: Colors.black38,
                                                style: BorderStyle.solid,
                                              )),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8.0)),
                                        ),
                                        child: Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            Image.asset(
                                              'assets/gifs/exercise/${exercise_day_one[index].animation}',
                                              scale: 1,
                                              fit: BoxFit.cover,
                                            ),
                                            Positioned(
                                              right: 0,
                                              bottom: 0,
                                              child: Container(
                                                width: 50,
                                                height: 30,
                                                decoration: const BoxDecoration(
                                                  color: Colors.black38,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topLeft: Radius
                                                              .circular(6),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  6)),
                                                ),
                                                child: Center(
                                                  child: Text(
                                                    "${exercise_day_one[index].repeat} s",
                                                    style: const TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 5,
                              left: 2,
                              child: Container(
                                width: 18,
                                height: 18,
                                decoration: BoxDecoration(
                                  color: Colors.blue[700],
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(100)),
                                ),
                                child: const Icon(
                                  Icons.circle,
                                  size: 10,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ),
              Positioned(
                bottom: 30,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacementNamed(
                      context,
                      '/exercisepracticestart',
                      arguments: {
                        'exerciseList': exercise_day_one,
                        'exerciseFother': 'exercise',
                      },
                    );
                  },
                  style: const ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll(Color(0xFF1976D2)),
                      padding: MaterialStatePropertyAll(
                          EdgeInsets.symmetric(horizontal: 120, vertical: 10))),
                  child: SizedBox(
                    height: mediaQuery.height * 0.05,
                    child: const Center(
                      child: Text(
                        "START",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
