import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/db/model.dart';

class ExerciseItemDetail extends StatefulWidget {
  const ExerciseItemDetail({
    super.key,
  });
  @override
  State<ExerciseItemDetail> createState() => _ExerciseItemDetailState();
}

class _ExerciseItemDetailState extends State<ExerciseItemDetail> {
  int increase = 0;
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    final arguments = ModalRoute.of(context)?.settings.arguments as Map;
    bool replacebutton = arguments['replacebutton'];
    bool addbutton = arguments['addbutton'];
    String navigator = arguments['navigate'];
    String exerciseFother = arguments['exerciseFother'];
    Exercise? exerciseItem = arguments['exerciseItem'];
    List<Exercise>? exercise = arguments['exercise'];

    List<Exercise>? addnewList = [];
    if (exerciseItem != null) {
      addnewList.add(exerciseItem);
    }
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        flexibleSpace: const Padding(
          padding: EdgeInsets.only(left: 40, right: 40, top: 40),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Animation",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
              Text(
                "Video",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
      body: exercise == null
          ? Container(
              width: mediaQuery.width,
              height: mediaQuery.height,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  exerciseItem != null
                      ? Container(
                          width: mediaQuery.width,
                          height: mediaQuery.height * 0.26,
                          decoration: const BoxDecoration(
                            color: Colors.black12,
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                          ),
                          child: Image.asset(
                            'assets/gifs/$exerciseFother/${exerciseItem.animation}',
                            fit: BoxFit.cover,
                            scale: 1,
                          ),
                        )
                      : Container(
                          width: mediaQuery.width,
                          height: mediaQuery.height * 0.26,
                          decoration: const BoxDecoration(
                            color: Colors.black12,
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                  exerciseItem != null
                      ? Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Text(
                            exerciseItem.name,
                            textAlign: TextAlign.left,
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 24,
                              color: Colors.black,
                            ),
                          ),
                        )
                      : const Padding(
                          padding: EdgeInsets.symmetric(vertical: 15),
                          child: Text(
                            'None',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 24,
                              color: Colors.black,
                            ),
                          ),
                        ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                        "Duration",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        "00:${exerciseItem?.repeat}",
                        style: const TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: mediaQuery.width,
                    height: mediaQuery.height * 0.4,
                    child: const Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Text(
                            "Start with your feet togethers and your arms by your side, then jump up with your feet apqrt and your hands overhead.",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Text(
                            "Return to the start position then do the next rep, This exercise providers a full-body workout and works all your large muscle groups.",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (!replacebutton)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  size: 20,
                                  color: Colors.black54,
                                ),
                              ),
                              Text(
                                '${exerciseItem!.id}/18',
                                style: const TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                              IconButton(
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.arrow_forward_ios,
                                  size: 20,
                                  color: Colors.black54,
                                ),
                              ),
                            ],
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.pushReplacementNamed(
                                  context, navigator);
                            },
                            style: const ButtonStyle(
                              backgroundColor:
                                  MaterialStatePropertyAll(Color(0xFF1976D2)),
                              padding: MaterialStatePropertyAll(
                                  EdgeInsets.symmetric(
                                      horizontal: 40, vertical: 10)),
                              side: MaterialStatePropertyAll(
                                BorderSide(width: 2, color: Color(0xFF1976D2)),
                              ),
                            ),
                            child: SizedBox(
                              height: mediaQuery.height * 0.05,
                              child: const Center(
                                child: Text(
                                  "CLOSE",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  if (replacebutton)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              Navigator.pushReplacementNamed(
                                context,
                                navigator,
                              );
                            },
                            style: const ButtonStyle(
                              backgroundColor:
                                  MaterialStatePropertyAll(Color(0xFFFFFFFF)),
                              padding: MaterialStatePropertyAll(
                                  EdgeInsets.symmetric(
                                      horizontal: 40, vertical: 10)),
                              side: MaterialStatePropertyAll(
                                BorderSide(width: 2, color: Color(0xFF1976D2)),
                              ),
                            ),
                            child: SizedBox(
                              height: mediaQuery.height * 0.05,
                              child: const Center(
                                child: Text(
                                  "CLOSE",
                                  style: TextStyle(
                                    color: Color(0xFF1976D2),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          if (addbutton)
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pushReplacementNamed(
                                    context, '/exerciseaddnew',
                                    arguments: {
                                      'addnewList': addnewList,
                                    });
                              },
                              style: const ButtonStyle(
                                backgroundColor:
                                    MaterialStatePropertyAll(Color(0xFF1976D2)),
                                padding: MaterialStatePropertyAll(
                                    EdgeInsets.symmetric(
                                        horizontal: 40, vertical: 10)),
                                side: MaterialStatePropertyAll(
                                  BorderSide(
                                      width: 2, color: Color(0xFF1976D2)),
                                ),
                              ),
                              child: SizedBox(
                                height: mediaQuery.height * 0.05,
                                child: const Center(
                                  child: Text(
                                    "ADD",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          if (!addbutton)
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pushReplacementNamed(
                                    context, '/daily-exercises');
                              },
                              style: const ButtonStyle(
                                backgroundColor:
                                    MaterialStatePropertyAll(Color(0xFF1976D2)),
                                padding: MaterialStatePropertyAll(
                                    EdgeInsets.symmetric(
                                        horizontal: 40, vertical: 10)),
                                side: MaterialStatePropertyAll(
                                  BorderSide(
                                      width: 2, color: Color(0xFF1976D2)),
                                ),
                              ),
                              child: SizedBox(
                                height: mediaQuery.height * 0.05,
                                child: const Center(
                                  child: Text(
                                    "REPLACE",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                ],
              ),
            )
          : Container(
              width: mediaQuery.width,
              height: mediaQuery.height,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  exerciseItem != null
                      ? Container(
                          width: mediaQuery.width,
                          height: mediaQuery.height * 0.26,
                          decoration: const BoxDecoration(
                            color: Colors.black12,
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                          ),
                          child: Image.asset(
                            'assets/gifs/$exerciseFother/${exercise[increase].animation}',
                            fit: BoxFit.cover,
                            scale: 1,
                          ),
                        )
                      : Container(
                          width: mediaQuery.width,
                          height: mediaQuery.height * 0.26,
                          decoration: const BoxDecoration(
                            color: Colors.black12,
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15),
                    child: Text(
                      exercise[increase].name,
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 24,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                        "Duration",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        "00:${exerciseItem?.repeat}",
                        style: const TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: mediaQuery.width,
                    height: mediaQuery.height * 0.4,
                    child: const Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Text(
                            "Start with your feet togethers and your arms by your side, then jump up with your feet apqrt and your hands overhead.",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Text(
                            "Return to the start position then do the next rep, This exercise providers a full-body workout and works all your large muscle groups.",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (!replacebutton)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              if (increase > 0)
                                IconButton(
                                  onPressed: () {
                                    setState(() {
                                      increase--;
                                    });
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back_ios,
                                    size: 20,
                                    color: Colors.black54,
                                  ),
                                ),
                              Text(
                                '${exercise[increase]!.id}/${exercise.length}',
                                style: const TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                              if (increase < exercise.length - 1)
                                IconButton(
                                  onPressed: () {
                                    setState(() {
                                      increase++;
                                    });
                                  },
                                  icon: const Icon(
                                    Icons.arrow_forward_ios,
                                    size: 20,
                                    color: Colors.black54,
                                  ),
                                ),
                            ],
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.pushReplacementNamed(
                                  context, navigator);
                            },
                            style: const ButtonStyle(
                              backgroundColor:
                                  MaterialStatePropertyAll(Color(0xFF1976D2)),
                              padding: MaterialStatePropertyAll(
                                  EdgeInsets.symmetric(
                                      horizontal: 40, vertical: 10)),
                              side: MaterialStatePropertyAll(
                                BorderSide(width: 2, color: Color(0xFF1976D2)),
                              ),
                            ),
                            child: SizedBox(
                              height: mediaQuery.height * 0.05,
                              child: const Center(
                                child: Text(
                                  "CLOSE",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  if (replacebutton)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              Navigator.pushReplacementNamed(
                                context,
                                navigator,
                              );
                            },
                            style: const ButtonStyle(
                              backgroundColor:
                                  MaterialStatePropertyAll(Color(0xFFFFFFFF)),
                              padding: MaterialStatePropertyAll(
                                  EdgeInsets.symmetric(
                                      horizontal: 40, vertical: 10)),
                              side: MaterialStatePropertyAll(
                                BorderSide(width: 2, color: Color(0xFF1976D2)),
                              ),
                            ),
                            child: SizedBox(
                              height: mediaQuery.height * 0.05,
                              child: const Center(
                                child: Text(
                                  "CLOSE",
                                  style: TextStyle(
                                    color: Color(0xFF1976D2),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          if (addbutton)
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pushReplacementNamed(
                                    context, '/exerciseaddnew',
                                    arguments: {
                                      'addnewList': addnewList,
                                    });
                              },
                              style: const ButtonStyle(
                                backgroundColor:
                                    MaterialStatePropertyAll(Color(0xFF1976D2)),
                                padding: MaterialStatePropertyAll(
                                    EdgeInsets.symmetric(
                                        horizontal: 40, vertical: 10)),
                                side: MaterialStatePropertyAll(
                                  BorderSide(
                                      width: 2, color: Color(0xFF1976D2)),
                                ),
                              ),
                              child: SizedBox(
                                height: mediaQuery.height * 0.05,
                                child: const Center(
                                  child: Text(
                                    "ADD",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          if (!addbutton)
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pushReplacementNamed(
                                    context, '/daily-exercises');
                              },
                              style: const ButtonStyle(
                                backgroundColor:
                                    MaterialStatePropertyAll(Color(0xFF1976D2)),
                                padding: MaterialStatePropertyAll(
                                    EdgeInsets.symmetric(
                                        horizontal: 40, vertical: 10)),
                                side: MaterialStatePropertyAll(
                                  BorderSide(
                                      width: 2, color: Color(0xFF1976D2)),
                                ),
                              ),
                              child: SizedBox(
                                height: mediaQuery.height * 0.05,
                                child: const Center(
                                  child: Text(
                                    "REPLACE",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                ],
              ),
            ),
    );
  }
}
