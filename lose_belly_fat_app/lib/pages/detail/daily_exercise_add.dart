import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/api/controller/exercise_item_con.dart';
import 'package:lose_belly_fat_app/api/model/exercise_item.dart';
import 'package:lose_belly_fat_app/components/appbar_default.dart';
import 'package:lose_belly_fat_app/components/exercise_list_item.dart';
import 'package:lose_belly_fat_app/db/data.dart';

class ExerciseDailyAdd extends StatefulWidget {
  const ExerciseDailyAdd({super.key});

  @override
  State<ExerciseDailyAdd> createState() => _ExerciseDailyAddState();
}

class _ExerciseDailyAddState extends State<ExerciseDailyAdd> {
  var exerciseItemController = ExerciseItemController();
  Future<List<ExercisesItem>> fetchExercisesItem() async {
    List<ExercisesItem> exercisesItems = [];
    try {
      exercisesItems = await exerciseItemController.getExerciseItemList();
    } catch (e) {
      print('e: $e');
    }
    setState(() {
      exercisesItems = exercisesItems;
    });
    return exercisesItems;
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;

    return Material(
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: const Color(0xFFF9F9F9),
          appBar: AppBar(
            title: const AppBarDefault(
              name: "Add Exercise",
              navigator: "/daily-exercises",
              showReset: false,
              save: false,
              color: Colors.black,
            ),
            bottom: const TabBar(
              indicatorPadding: EdgeInsets.symmetric(horizontal: 10),
              unselectedLabelColor: Colors.black87,
              unselectedLabelStyle: TextStyle(color: Colors.black87),
              splashBorderRadius: BorderRadius.all(Radius.circular(20)),
              labelColor: Colors.white,
              indicatorSize: TabBarIndicatorSize.tab,
              automaticIndicatorColorAdjustment: false,
              indicator: BoxDecoration(
                  color: Colors.black87,
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              tabs: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8),
                  child: Text(
                    'Exercise',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8),
                  child: Text(
                    'Warm up',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8),
                  child: Text(
                    'Stretch',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Container(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.85,
                padding: const EdgeInsets.all(8.0),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 1,
                      color: Colors.black12,
                    ),
                  ),
                ),
                child: ListView.builder(
                  itemCount: exercise_list.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.pushReplacementNamed(
                          context,
                          '/exerciseitemdetail',
                          arguments: {
                            'navigate': '/exsercisedailyadd',
                            'replacebutton': true,
                            'addbutton': true,
                            'exerciseFother': 'exercise',
                            'exerciseItem': exercise_list[index],
                            'exercise': null,
                          },
                        );
                      },
                      child: ExerciseListItem(
                        mediaQuery: mediaQuery,
                        showReplace: true,
                        newExercise: false,
                        item: exercise_list[index],
                        imageGif: exercise_list[index].animation,
                      ),
                    );
                  },
                ),
              ),
              Container(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.85,
                padding: const EdgeInsets.all(8.0),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 1,
                      color: Colors.black12,
                    ),
                  ),
                ),
                child: FutureBuilder<List<ExercisesItem>>(
                  future: fetchExercisesItem(),
                  builder: (context, snapshot) {
                    return ListView.builder(
                      itemCount: snapshot.data!.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 20),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 3,
                                child: SizedBox(
                                  width: mediaQuery.width * 0.15,
                                  height: mediaQuery.height * 0.08,
                                  child: Image.asset(
                                    'assets/gifs/exercise/${snapshot.data?[index].animation}',
                                    scale: 1,
                                    fit: BoxFit.cover,
                                  ),
                                  // Container(
                                  // width: mediaQuery.width * 0.15,
                                  // height: mediaQuery.height * 0.08,
                                ),
                              ),
                              const Expanded(
                                flex: 5,
                                child: Padding(
                                  padding: EdgeInsets.only(left: 20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Text(
                                        'widget.item!.repeat',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 13,
                                          color: Colors.black45,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Center(
                                  child: IconButton(
                                    onPressed: () {},
                                    style: const ButtonStyle(
                                        side: MaterialStatePropertyAll(
                                          BorderSide(
                                            width: 3,
                                            color: Colors.black12,
                                          ),
                                        ),
                                        iconSize: MaterialStatePropertyAll(8),
                                        minimumSize: MaterialStatePropertyAll(
                                          Size(8, 8),
                                        )),
                                    icon: const Icon(
                                      Icons.add,
                                      size: 18,
                                      color: Color(0xFF0D47A1),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
              Container(
                width: mediaQuery.width,
                height: mediaQuery.height * 0.85,
                padding: const EdgeInsets.all(8.0),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 1,
                      color: Colors.black12,
                    ),
                  ),
                ),
                child: ListView.builder(
                  itemCount: exercise_list.length,
                  itemBuilder: (context, index) {
                    return ExerciseListItem(
                      mediaQuery: mediaQuery,
                      showReplace: true,
                      newExercise: false,
                      item: exercise_list[index],
                      imageGif: exercise_list[index].animation,
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
