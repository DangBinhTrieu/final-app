import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/components/appbar_default.dart';
import 'package:lose_belly_fat_app/components/exercise_list_item.dart';
import 'package:lose_belly_fat_app/db/model.dart';

class ExerciseAddNew extends StatefulWidget {
  const ExerciseAddNew({
    super.key,
  });

  @override
  State<ExerciseAddNew> createState() => _ExerciseAddNewState();
}

class _ExerciseAddNewState extends State<ExerciseAddNew> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    final arguments = ModalRoute.of(context)?.settings.arguments as Map;
    List<Exercise>? addnewList = arguments['addnewList'];
    List<Exercise>? exerArray = [];
    for (var element in addnewList!) {
      exerArray.add(element);
    }
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const AppBarDefault(
          name: "New training",
          navigator: "/exsercisedailyadd",
          showReset: true,
          save: true,
          color: Colors.white,
        ),
      ),
      body: Column(
        children: [
          Expanded(
            flex: 8,
            // ignore: unnecessary_null_comparison
            child: addnewList != null
                ? SizedBox(
                    width: mediaQuery.width,
                    height: mediaQuery.height,
                    child: ListView.builder(
                        itemCount: exerArray.length,
                        itemBuilder: (context, index) {
                          return ExerciseListItem(
                            mediaQuery: mediaQuery,
                            showReplace: false,
                            newExercise: true,
                            item: exerArray[index],
                            imageGif: '',
                          );
                        }),
                  )
                : SizedBox(
                    width: mediaQuery.width,
                    height: mediaQuery.height,
                  ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 18),
              width: mediaQuery.width * 0.8,
              height: mediaQuery.height * 0.02,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/exsercisedailyadd');
                },
                style: const ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(Color(0xFF1976D2)),
                  padding: MaterialStatePropertyAll(
                      EdgeInsets.symmetric(horizontal: 40)),
                  side: MaterialStatePropertyAll(
                    BorderSide(width: 2, color: Color(0xFF1976D2)),
                  ),
                ),
                child: SizedBox(
                  height: mediaQuery.height * 0.05,
                  child: const Center(
                    child: Text(
                      "Add Exercises",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
