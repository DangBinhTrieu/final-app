import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppBarDefaultDis extends StatelessWidget {
  const AppBarDefaultDis({
    super.key,
    required this.mediaQuery,
    required this.images,
    required this.title,
    required this.subTitle,
    required this.navigate,
  });

  final Size mediaQuery;
  final String images;
  final String title;
  final String subTitle;
  final String navigate;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Positioned(
          bottom: -10,
          right: 0,
          child: Image.asset(
            './assets/images/${images}',
            fit: BoxFit.cover,
            repeat: ImageRepeat.noRepeat,
            width: 200,
            height: 200,
          ),
          // SvgPicture.asset(
          //   images,
          //   fit: BoxFit.cover,
          //   width: 200,
          //   height: 200,
          // ),
        ),
        Positioned(
          left: 20,
          child: SizedBox(
            width: mediaQuery.width * 0.7,
            height: mediaQuery.height * 0.25,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text(
                  title,
                  style: const TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 28,
                    color: Colors.white,
                  ),
                ),
                Text(
                  subTitle,
                  style: const TextStyle(
                    color: Colors.white70,
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
          top: 10,
          left: 10,
          child: InkWell(
            onTap: () {
              Navigator.pushReplacementNamed(context, navigate);
            },
            child: const Icon(
              Icons.arrow_back,
              size: 20,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
