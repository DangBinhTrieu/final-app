import 'package:flutter/material.dart';
import 'package:lose_belly_fat_app/db/model.dart';
// import 'package:flutter_gif/flutter_gif.dart';

class ExerciseListItem extends StatefulWidget {
  const ExerciseListItem({
    super.key,
    required this.mediaQuery,
    required this.showReplace,
    required this.newExercise,
    required this.imageGif,
    required this.item,
  });

  final Size mediaQuery;
  final bool showReplace;
  final bool newExercise;
  final String imageGif;
  final Exercise? item;

  @override
  State<ExerciseListItem> createState() => _ExerciseListItemState();
}

class _ExerciseListItemState extends State<ExerciseListItem> {
  @override
  Widget build(BuildContext context) {
    if (widget.showReplace) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Row(
          children: [
            Expanded(
              flex: 3,
              // ignore: unnecessary_null_comparison
              child: widget.imageGif != null
                  ? SizedBox(
                      width: widget.mediaQuery.width * 0.15,
                      height: widget.mediaQuery.height * 0.08,
                      child: Image.asset(
                        'assets/gifs/exercise/${widget.item!.animation}',
                        scale: 1,
                        fit: BoxFit.cover,
                      ),
                    )
                  : SizedBox(
                      width: widget.mediaQuery.width * 0.15,
                      height: widget.mediaQuery.height * 0.08,
                    ),
            ),
            Expanded(
              flex: 5,
              child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.item!.name,
                      style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      widget.item!.repeat,
                      style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 13,
                        color: Colors.black45,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Center(
                child: IconButton(
                  onPressed: () {},
                  style: const ButtonStyle(
                      side: MaterialStatePropertyAll(
                        BorderSide(
                          width: 3,
                          color: Colors.black12,
                        ),
                      ),
                      iconSize: MaterialStatePropertyAll(8),
                      minimumSize: MaterialStatePropertyAll(
                        Size(8, 8),
                      )),
                  icon: const Icon(
                    Icons.add,
                    size: 18,
                    color: Color(0xFF0D47A1),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Row(
          children: [
            const Expanded(
              flex: 1,
              child: Icon(
                Icons.dehaze,
                size: 18,
                color: Colors.black54,
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                width: widget.mediaQuery.width * 0.15,
                height: widget.mediaQuery.height * 0.08,
                decoration: const BoxDecoration(
                  color: Colors.black12,
                ),
                child: Image.asset(
                  'assets/gifs/exercise/${widget.item!.animation}',
                  scale: 1,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.item!.name,
                      style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      widget.item!.repeat,
                      style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 13,
                        color: Colors.black45,
                      ),
                    )
                  ],
                ),
              ),
            ),
            if (!widget.newExercise)
              Expanded(
                flex: 1,
                child: IconButton(
                  onPressed: () {
                    Navigator.pushReplacementNamed(
                      context,
                      '/exerciseitemreplace',
                      arguments: {
                        'exerciseItem': widget.item,
                      },
                    );
                  },
                  icon: const Icon(
                    Icons.swap_horiz_rounded,
                    size: 18,
                    color: Colors.black54,
                  ),
                ),
              ),
            if (widget.newExercise)
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: TextButton(
                    onPressed: () {},
                    style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.red),
                      shape: MaterialStatePropertyAll(
                        CircleBorder(),
                      ),
                    ),
                    child: const Text(
                      "-",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 28,
                        color: Colors.white,
                        // backgroundColor: Colors.red,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
              ),
          ],
        ),
      );
    }
  }
}
