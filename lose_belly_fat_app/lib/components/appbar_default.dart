import 'package:flutter/material.dart';

class AppBarDefault extends StatefulWidget {
  const AppBarDefault({
    super.key,
    required this.name,
    required this.navigator,
    required this.showReset,
    required this.save,
    required this.color,
  });
  final String name;
  final String navigator;
  final bool showReset;
  final bool save;
  final Color color;

  @override
  State<AppBarDefault> createState() => _AppBarDefaultState();
}

class _AppBarDefaultState extends State<AppBarDefault> {
  @override
  Widget build(BuildContext context) {
    return widget.showReset
        ? Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pushReplacementNamed(
                        context,
                        widget.navigator,
                        arguments: {'title': 'Detail Exercise'},
                      );
                    },
                    child: Icon(
                      Icons.arrow_back,
                      size: 20,
                      color: widget.color,
                    ),
                  ),
                  const SizedBox(width: 25),
                  Text(
                    widget.name,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                      color: widget.color,
                    ),
                  ),
                ],
              ),
              if (!widget.save)
                Text(
                  "Reset",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                    color: Colors.blue[700],
                  ),
                ),
              if (widget.save)
                TextButton(
                  onPressed: () {},
                  child: const Text(
                    "Save",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pushReplacementNamed(
                        context,
                        '/daily-exercises',
                        arguments: {'title': 'Detail Exercise'},
                      );
                    },
                    child: const Icon(
                      Icons.arrow_back,
                      size: 20,
                      color: Colors.black,
                    ),
                  ),
                  const SizedBox(width: 25),
                  Text(
                    widget.name,
                    style: const TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ],
          );
  }
}
