import 'package:flutter/material.dart';

// ignore: must_be_immutable
class StartButtonWidget extends StatelessWidget {
  const StartButtonWidget({
    super.key,
    required this.mediaQuery,
    required this.name,
    required this.navigatorFunction,
    required this.bgColor,
    required this.txtColor,
  });

  // ignore: prefer_typing_uninitialized_variables
  final mediaQuery;
  final String name;
  final VoidCallback? navigatorFunction;
  final Color bgColor;
  final Color txtColor;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: navigatorFunction,
      style: ButtonStyle(
        backgroundColor: MaterialStatePropertyAll(bgColor),
        padding: const MaterialStatePropertyAll(
            EdgeInsets.symmetric(horizontal: 40, vertical: 10)),
        side: const MaterialStatePropertyAll(
          BorderSide(width: 2, color: Color(0xFF1976D2)),
        ),
      ),
      child: SizedBox(
        height: mediaQuery,
        child: Center(
          child: Text(
            name,
            style: TextStyle(
              color: txtColor,
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}
