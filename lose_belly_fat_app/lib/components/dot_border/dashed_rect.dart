import 'package:flutter/material.dart';
import 'dart:math' as math;

class DashedRect extends StatelessWidget {
  final Color color;
  final double strokeWidth;
  final double gap;

  const DashedRect({
    super.key,
    this.color = Colors.black,
    this.strokeWidth = 1.0,
    this.gap = 5.0,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(strokeWidth / 2),
      child: CustomPaint(
        painter: DashRectPainter(
          color: color,
          strokeWidth: strokeWidth,
          gap: gap,
        ),
      ),
    );
  }
}

class DashRectPainter extends CustomPainter {
  final double strokeWidth;
  final Color color;
  final double gap;

  DashRectPainter({
    this.strokeWidth = 5.0,
    this.color = Colors.red,
    this.gap = 5.0,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Paint dashedPaint = Paint()
      ..color = color
      ..strokeWidth = strokeWidth
      ..style = PaintingStyle.stroke;

    double x = size.width;
    double y = size.height;

    getDashedPath(
      a: const math.Point(0, 0),
      b: math.Point(x, 0),
      gap: gap,
    );
    getDashedPath(
      a: math.Point(x, 0),
      b: math.Point(x, y),
      gap: gap,
    );
    getDashedPath(
      a: math.Point(0, y),
      b: math.Point(x, y),
      gap: gap,
    );
    Path leftPath = getDashedPath(
      a: const math.Point(0, 0),
      b: math.Point(0.001, y * 0.35),
      gap: gap,
    );

    // canvas.drawPath(_topPath, dashedPaint);
    // canvas.drawPath(_rightPath, dashedPaint);
    // canvas.drawPath(_bottomPath, dashedPaint);
    canvas.drawPath(leftPath, dashedPaint);
  }

  Path getDashedPath({
    required math.Point<double> a,
    required math.Point<double> b,
    required double gap,
  }) {
    Path path = Path();
    double distance = a.distanceTo(b);
    double dx = (b.x - a.x) * gap / distance;
    double dy = (b.y - a.y) * gap / distance;

    for (double i = 0; i < distance; i += gap) {
      path.moveTo(a.x + dx * i, a.y + dy * i);
      path.lineTo(a.x + dx * (i + gap / 2), a.y + dy * (i + gap / 2));
    }
    return path;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
