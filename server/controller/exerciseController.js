const { Exercises } = require("../model/exerciseModel");

const { ExerciseType } = require("../model/exerciseTypeModel");

const exercisesController = {
  //ADD A BOOK
  addAExercise: async (req, res) => {
    try {
      const newExercises = new Exercises(req.body);
      const savedExercise = await newExercises.save();

      if (req.body.exerciseType) {
        const exerciseType = ExerciseType.findById(req.body.exerciseType);
        await exerciseType.updateOne({
          $push: { exercises: savedExercise._id },
        });
      }

      res.status(200).json(savedExercise);
    } catch (err) {
      res.status(500).json(err);
    }
  },
  //GET ALL BOOKS
  getAllExercises: async (req, res) => {
    try {
      const allExercises = await Exercises.find();
      res.status(200).json(allExercises);
    } catch (err) {
      res.status(500).json(err);
    }
  },

  //GET A BOOK
  getAExercise: async (req, res) => {
    try {
      const exercises = await Exercises.findById(req.params.id).populate([
        "exerciseType",
      ]);
      res.status(200).json(exercises);
    } catch (err) {
      res.status(500).json(err);
    }
  },

  //UPDATE BOOK
  updateExercise: async (req, res) => {
    try {
      const exercise = await Exercises.findById(req.params.id);
      await exercise.updateOne({ $set: req.body });
      res.status(200).json("Updated successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  },

  //DELETE BOOK
  deleteExercise: async (req, res) => {
    try {
      await ExerciseType.updateMany(
        { exercises: req.params.id },
        { $pull: { exercises: req.params.id } }
      );

      await Exercises.findByIdAndDelete(req.params.id);
      res.status(200).json("Deleted successfully");
    } catch (err) {
      res.status(500).json(err);
    }
  },
};

module.exports = exercisesController;
