const { ExerciseType } = require("../model/exerciseTypeModel");
const { Exercises } = require("../model/exerciseModel");

const exerciseTypeController = {
  //ADD AUTHOR
  addExerciseType: async (req, res) => {
    try {
      const newExerciseType = new ExerciseType(req.body);
      const savedExerciseType = await newExerciseType.save();
      res.status(200).json(savedExerciseType);
    } catch (err) {
      res.status(500).json(err); //HTTP REQUEST CODE
    }
  },

  //GET ALL AUTHORS
  getAllExerciseTypes: async (req, res) => {
    try {
      const exerciseTypes = await ExerciseType.find();
      res.status(200).json(exerciseTypes);
    } catch (err) {
      res.status(500).json(err);
    }
  },

  //GET AN AUTHOR
  getAnExerciseType: async (req, res) => {
    try {
      const exerciseType = await ExerciseType.findById(req.params.id).populate(
        "exercises"
      );
      res.status(200).json(exerciseType);
    } catch (err) {
      res.status(500).json(err);
    }
  },

  //UPDATE AUTHOR
  updateExerciseType: async (req, res) => {
    try {
      const exerciseType = await ExerciseType.findById(req.params.id);
      await exerciseType.updateOne({ $set: req.body });
      res.status(200).json("Updated successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  },

  //DELETE AUTHOR
  deleteExerciseType: async (req, res) => {
    try {
      await Exercises.updateMany(
        { exerciseType: req.params.id },
        { exerciseType: null }
      );
      res.status(200).json("Deleted successfully!");
    } catch (err) {
      res.status(500).json(err);
    }
  },
};

module.exports = exerciseTypeController;
