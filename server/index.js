const express = require("express");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
var bodyParser = require("body-parser");
const morgan = require("morgan");
const dotenv = require("dotenv");
// const multer = require("multer");
// const jwt = require("jsonwebtoken");
// const path = require("path");

const exerciseRoute = require("./route/Exercise");
const exerciseTypeRoute = require("./route/ExerciseType");

dotenv.config();

//CONECTING DB// APP CONFI
mongoose.connect(
  "mongodb+srv://trieu93710:trieu93710@cluster0.egh6hjf.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0",
  {
    useNewUrlParser: true,
  }
);

app.use(express.json());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());
app.use(morgan("common"));

//ROUTES
app.use("/v1/exercise", exerciseRoute);
app.use("/v1/exerciseType", exerciseTypeRoute);

app.listen(process.env.PORT || 8000, () => {
  console.log("Server is running...");
});
