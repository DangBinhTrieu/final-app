const exerciseTypeController = require("../controller/exerciseTypeController");

const router = require("express").Router();

//ADD AUTHOR
router.post("/", exerciseTypeController.addExerciseType);

//GET ALL AUTHORS
router.get("/", exerciseTypeController.getAllExerciseTypes);

//GET AN AUTHOR
router.get("/:id", exerciseTypeController.getAnExerciseType);

//UPDATE AN AUTHOR
router.put("/:id", exerciseTypeController.updateExerciseType);

//DELETE AUTHOR
router.delete("/:id", exerciseTypeController.deleteExerciseType);

module.exports = router;
