const exercisesController = require("../controller/exerciseController");

const router = require("express").Router();

//ADD A BOOK
router.post("/", exercisesController.addAExercise);

//GET ALL BOOKS
router.get("/", exercisesController.getAllExercises);

//GET A BOOK
router.get("/:id", exercisesController.getAExercise);

//UPDATE A BOOK
router.put("/:id", exercisesController.updateExercise);

//DELETE A BOOK
router.delete("/:id", exercisesController.deleteExercise);

module.exports = router;
