const mongoose = require("mongoose");
// const { v4: uuidv4 } = require("uuid");

const exercisesSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  durian: {
    type: Array,
  },
  description: {
    type: String,
  },
  animation: {
    type: String,
  },
  video: {
    type: String,
  },
  exerciseType: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "ExerciseType",
  },
});

let Exercises = mongoose.model("Exercises", exercisesSchema);

module.exports = { Exercises };
