const mongoose = require("mongoose");

const ExerciseTypeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  exercises: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Exercises",
    },
  ],
});

let ExerciseType = mongoose.model("ExerciseType", ExerciseTypeSchema);

module.exports = { ExerciseType };
